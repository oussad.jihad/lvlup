# LvLup

Application web de l'organisme de formation LvLup

**State build**: [![pipeline status](https://gitlab.com/oussad.jihad/lvlup/badges/master/pipeline.svg)](https://gitlab.com/oussad.jihad/lvlup/-/commits/master)

**Jacoco report**: [![jacoco report](https://gitlab.com/oussad.jihad/lvlup/badges/master/coverage.svg)](https://gitlab.com/oussad.jihad/lvlup/-/commits/master)
