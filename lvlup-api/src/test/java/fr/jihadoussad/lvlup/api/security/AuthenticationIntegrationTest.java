package fr.jihadoussad.lvlup.api.security;

import fr.jihadoussad.lvlup.api.context.LvlupApiContext;
import fr.jihadoussad.lvlup.core.entities.User;
import fr.jihadoussad.lvlup.core.repositories.UserRepository;
import fr.jihadoussad.tokenserver.core.repositories.InvalidTokenRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;

import static fr.jihadoussad.lvlup.api.LvlUpApplication.PASSWORD;
import static fr.jihadoussad.lvlup.api.LvlUpApplication.USERNAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class AuthenticationIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private LvlupApiContext context;

    @Resource
    private InvalidTokenRepository invalidTokenRepository;

    @Resource
    private UserRepository userRepository;

    @BeforeEach
    public void clearContextAuthentication() {
        userRepository.findByLogin(USERNAME).ifPresent(user -> {
            user.setAuthenticationRetry(0);
            userRepository.saveAndFlush(user);
        });
    }

    @Test
    public void loginWithNoCredentialsTest() throws Exception {
        mvc.perform(post("/login")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void loginWhenUserNotFoundTest() throws Exception {
        mvc.perform(post("/login")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("username", "unknownUser")
                        .param("password", "wrongPassword"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void loginWhenWrongPasswordTest() throws Exception {
        mvc.perform(post("/login")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("username", USERNAME)
                        .param("password", "wrongPassword"))
                .andExpect(status().isUnauthorized());

        assertThat(userRepository.findByLogin(USERNAME)).isNotEmpty().get()
                .extracting(User::getAuthenticationRetry).isEqualTo(1);
    }

    @Test
    public void loginWhenMaxRetryAuthenticationTest() throws Exception {
        final int originalMaxRetry = context.getMaxRetry();
        context.setMaxRetry(1);
        mvc.perform(post("/login")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("username", USERNAME)
                        .param("password", "wrongPassword"))
                .andExpect(status().isUnauthorized());

        assertThat(userRepository.findByLogin(USERNAME)).isNotEmpty().get()
                .extracting(User::getAuthenticationRetry).isEqualTo(1);

        mvc.perform(post("/login")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("username", USERNAME)
                        .param("password", PASSWORD))
                .andExpect(status().isUnauthorized());

        context.setMaxRetry(originalMaxRetry);
    }

    @Test
    public void logoutWhenNoAccessTokenCookieTest() throws Exception {
        mvc.perform(post("/logout")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void loginAndLogoutTest() throws Exception {
        final Cookie cookie = mvc.perform(post("/login")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("username", USERNAME)
                        .param("password", PASSWORD))
                .andExpect(status().isOk())
                .andExpect(header().exists(HttpHeaders.SET_COOKIE))
                .andExpect(header().string(HttpHeaders.SET_COOKIE, containsString("SameSite=None")))
                .andReturn().getResponse().getCookie(context.getAccessTokenCookieName());

        assertThat(userRepository.findByLogin(USERNAME)).isNotEmpty().get()
                .extracting(User::getAuthenticationRetry).isEqualTo(0);
        assertThat(cookie).isNotNull();
        assertThat(cookie.getValue()).isNotEmpty();
        assertThat(cookie.getSecure()).isTrue();
        assertThat(cookie.isHttpOnly()).isTrue();
        assertThat(cookie.getPath()).isEqualTo("/");

        final MockHttpServletResponse response =
                mvc.perform(post("/logout")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(cookie)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn().getResponse();

        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
        assertThat(invalidTokenRepository.findInvalidTokenByToken(cookie.getValue())).isNotNull();
        assertThat(response.getCookie(context.getAccessTokenCookieName()))
                .isNotNull().extracting(Cookie::getMaxAge).isEqualTo(0);

        mvc.perform(delete("/admin/formation/" + 10)
                        .servletPath("/admin/formation/" + 10)
                        .header("Access-Control-Request-Method", "DELETE")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(cookie)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

}
