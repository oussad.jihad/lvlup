package fr.jihadoussad.lvlup.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.lvlup.api.context.LvlupApiContext;
import fr.jihadoussad.lvlup.contract.Registration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;

import static fr.jihadoussad.tools.api.response.ResponseCode.MISSING_MANDATORY_FIELD;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class RegistrationControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private LvlupApiContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void sendMailRegistrationWithOriginNotAllowedTest() throws Exception {
        mvc.perform(post("/send")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://lvlup-hacker.com")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void sendMailRegistrationWithGetHttpMethodTest() throws Exception {
        mvc.perform(get("/send")
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void sendMailRegistrationWithNoContentTest() throws Exception {
        mvc.perform(post("/send")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void sendMailRegistrationWhenContractViolationTest() throws Exception {
        final String message  =
                objectMapper.readValue(
                        mvc.perform(post("/send")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(new Registration())))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8), String.class);

       assertThat(message).isEqualTo(MISSING_MANDATORY_FIELD.message + "formation null");
    }

    @Test
    public void sendMailRegistrationWhenAMQPDownTest() throws Exception {
        final Registration registration = new Registration();
        registration.setFormation("formation");
        registration.setSessionDate("décembre 2021");
        registration.setLastname("lastname");
        registration.setFirstname("firstname");
        registration.setEmail("test@test.fr");
        registration.setPhoneNumber("0707070707");

        mvc.perform(post("/send")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(registration)))
                .andExpect(status().isInternalServerError());
    }
}
