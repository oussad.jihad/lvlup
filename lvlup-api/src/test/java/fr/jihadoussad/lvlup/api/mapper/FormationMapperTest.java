package fr.jihadoussad.lvlup.api.mapper;

import fr.jihadoussad.lvlup.core.entities.Formation;
import fr.jihadoussad.lvlup.core.entities.Image;
import fr.jihadoussad.lvlup.core.entities.Pdf;
import fr.jihadoussad.lvlup.core.entities.Point;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class FormationMapperTest {

    @Autowired
    private FormationMapper formationMapper;

    @Test
    public void coreToContractTest() {
        final Formation coreFormation = new Formation();
        coreFormation.setNom("formation");
        coreFormation.setPhrase("phrase");
        coreFormation.setPrix(1000);
        coreFormation.setImage(new Image());
        coreFormation.setPdf(new Pdf());
        final Point point = new Point();
        point.setContent("content");
        coreFormation.addPoint(point);

        final fr.jihadoussad.lvlup.contract.Formation contractFormation = formationMapper.coreToContract(coreFormation);

        assertThat(contractFormation).isNotNull();
        assertThat(contractFormation.getId()).isNull();
        assertThat(contractFormation.getNom()).isEqualTo(coreFormation.getNom());
        assertThat(contractFormation.getPhrase()).isEqualTo(coreFormation.getPhrase());
        assertThat(contractFormation.getPrix()).isEqualTo(coreFormation.getPrix());
        assertThat(contractFormation.getPoints()).isNotNull().hasSize(1)
                .containsExactly(point.getContent());
        assertThat(contractFormation.getImageId()).isNull();
        assertThat(contractFormation.getPdfId()).isNull();
    }

    @Test
    public void contractToCoreTest() {
        final fr.jihadoussad.lvlup.contract.Formation contractFormation = new fr.jihadoussad.lvlup.contract.Formation();
        contractFormation.setNom("formation");
        contractFormation.setPhrase("phrase");
        contractFormation.setPrix(1000);
        contractFormation.setPoints(List.of("content"));

        final Formation coreFormation = formationMapper.contractToCore(contractFormation, new Image(), new Pdf());

        assertThat(coreFormation).isNotNull();
        assertThat(coreFormation.getId()).isNull();
        assertThat(coreFormation.getNom()).isEqualTo(contractFormation.getNom());
        assertThat(coreFormation.getPhrase()).isEqualTo(contractFormation.getPhrase());
        assertThat(coreFormation.getPrix()).isEqualTo(contractFormation.getPrix());
        assertThat(coreFormation.getPoints()).isNotNull().hasSize(1)
                .extracting(Point::getContent).containsExactly("content");
        assertThat(coreFormation.getImage()).isNotNull();
        assertThat(coreFormation.getImage().getFile()).isNull();
        assertThat(coreFormation.getPdf()).isNotNull();
        assertThat(coreFormation.getPdf().getFile()).isNull();
    }
}
