package fr.jihadoussad.lvlup.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.lvlup.api.LvlUpApplication;
import fr.jihadoussad.lvlup.api.context.LvlupApiContext;
import fr.jihadoussad.lvlup.contract.Formation;
import fr.jihadoussad.lvlup.contract.ResponseCode;
import fr.jihadoussad.lvlup.contract.Session;
import fr.jihadoussad.lvlup.core.entities.Point;
import fr.jihadoussad.lvlup.core.repositories.FormationRepository;
import fr.jihadoussad.lvlup.core.repositories.ImageRepository;
import fr.jihadoussad.lvlup.core.repositories.PdfRepository;
import fr.jihadoussad.lvlup.core.repositories.SessionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.transaction.Transactional;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.List;

import static fr.jihadoussad.lvlup.api.LvlUpApplication.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class AdminControllerIntegrationTest {

    private static final String NEW_FORMATION_NAME = "Covid";
    private static final int NEW_FORMATION_PRICE = 2800;
    private static final String PHRASE = "New phrase";
    public static final String FIRST_PARAGRAPHE = "Un nouveau paragraphe";
    public static final String SECOND_PARAGRAPHE = "Puis un autre...";

    @Autowired
    private MockMvc mvc;

    @Autowired
    private LvlupApiContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @Resource
    private FormationRepository formationRepository;

    @Resource
    private ImageRepository imageRepository;

    @Resource
    private PdfRepository pdfRepository;

    @Resource
    private SessionRepository sessionRepository;

    private MockMultipartFile image;

    private MockMultipartFile pdf;

    private Formation formation;

    private Cookie accessTokenCookie;

    @BeforeEach
    public void setUp() throws Exception {
        this.image = new MockMultipartFile(
                "image",
                "Montessori.jpg",
                MediaType.IMAGE_JPEG_VALUE,
                new FileInputStream(context.getBaseResourceUrl() + "/img/Montessori.jpg").readAllBytes()
        );
        this.pdf = new MockMultipartFile(
                "pdf",
                "plaquette-formation-montessori.pdf",
                MediaType.IMAGE_JPEG_VALUE,
                new FileInputStream(context.getBaseResourceUrl() + "/pdf/plaquette-formation-montessori.pdf").readAllBytes()
        );
        this.formation = new Formation();
        this.accessTokenCookie = authenticateAndSendToken();
    }

    private Cookie authenticateAndSendToken() throws Exception {
        final Cookie cookie = mvc.perform(post("/login")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("username", USERNAME)
                        .param("password", PASSWORD))
                .andExpect(status().isOk()).andReturn().getResponse().getCookie(context.getAccessTokenCookieName());

        assertThat(cookie).isNotNull();
        assertThat(cookie.getValue()).isNotEmpty();

        return cookie;
    }

    @Test
    public void updateFormationWhenUserNotAuthenticatedTest() throws Exception {
        mvc.perform(get("/admin/formation")
                        .servletPath("/admin/formation")
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void updateFormationWithOriginNotAllowedTest() throws Exception {
        mvc.perform(put("/admin/formation")
                        .servletPath("/admin/formation")
                        .header("Access-Control-Request-Method", "PUT")
                        .header("Origin", "https://lvlup-hacker.com")
                        .cookie(authenticateAndSendToken())
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void updateFormationWithGetHttpMethodTest() throws Exception {
        mvc.perform(get("/admin/formation")
                        .servletPath("/admin/formation")
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie)
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void updateFormationWithNoContentTest() throws Exception {
        mvc.perform(put("/admin/formation")
                        .servletPath("/admin/formation")
                        .header("Access-Control-Request-Method", "PUT")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie)
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void updateFormationWhenFormationNotFoundTest() throws Exception {
        this.formation.setId(10L);

        final String message =
                objectMapper.readValue(
                        mvc.perform(multipart("/admin/formation")
                        .file("formation", this.objectMapper.writeValueAsString(this.formation).getBytes(StandardCharsets.UTF_8))
                        .with(request -> {request.setMethod(HttpMethod.PUT.name()); return request;})
                        .servletPath("/admin/formation")
                        .header("Access-Control-Request-Method", "PUT")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8), String.class);

        assertThat(message).isEqualTo(ResponseCode.FORMATION_NOT_EXIST.message);
    }

    @Test
    @Transactional
    public void updateFormationContentTest() throws Exception {
        this.formation.setId(formationRepository.findByNom(MONTESSORI_NAME).map(fr.jihadoussad.lvlup.core.entities.Formation::getId).orElseThrow());
        formation.setNom(NEW_FORMATION_NAME);
        formation.setPhrase(PHRASE);
        formation.setPoints(List.of(FIRST_PARAGRAPHE, SECOND_PARAGRAPHE));
        formation.setPrix(NEW_FORMATION_PRICE);

        mvc.perform(multipart("/admin/formation")
                        .file("formation", this.objectMapper.writeValueAsString(this.formation).getBytes(StandardCharsets.UTF_8))
                        .with(request -> {request.setMethod(HttpMethod.PUT.name()); return request;})
                        .servletPath("/admin/formation")
                        .header("Access-Control-Request-Method", "PUT")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie))
                .andExpect(status().isAccepted());

        final fr.jihadoussad.lvlup.core.entities.Formation coreFormation = formationRepository.findByNom(NEW_FORMATION_NAME).orElse(null);

        assertThat(coreFormation).isNotNull();
        assertThat(coreFormation.getPrix()).isEqualTo(NEW_FORMATION_PRICE);
        assertThat(coreFormation.getPoints()).isNotNull().extracting(Point::getContent).containsExactly(FIRST_PARAGRAPHE, SECOND_PARAGRAPHE);
        assertThat(coreFormation.getPdf()).isNotNull();
        assertThat(coreFormation.getImage()).isNotNull();

        coreFormation.setNom(MONTESSORI_NAME);
        coreFormation.setPrix(MONTESSORI_PRICE);
        coreFormation.getPoints().get(0).setContent(LvlUpApplication.FIRST_PARAGRAPHE);
        coreFormation.getPoints().get(1).setContent(LvlUpApplication.SECOND_PARAGRAPHE);
        this.formationRepository.saveAndFlush(coreFormation);
    }

    @Test
    public void updateFormationImageTest() throws Exception {
        this.formation.setId(formationRepository.findByNom(MONTESSORI_NAME).map(fr.jihadoussad.lvlup.core.entities.Formation::getId).orElseThrow());

        mvc.perform(multipart("/admin/formation")
                        .file("formation", this.objectMapper.writeValueAsString(this.formation).getBytes(StandardCharsets.UTF_8))
                        .file(image)
                        .with(request -> {request.setMethod(HttpMethod.PUT.name()); return request;})
                        .servletPath("/admin/formation")
                        .header("Access-Control-Request-Method", "PUT")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie))
                .andExpect(status().isAccepted());

        final fr.jihadoussad.lvlup.core.entities.Formation coreFormation = formationRepository.findByNom(MONTESSORI_NAME).orElse(null);

        assertThat(coreFormation).isNotNull();
        assertThat(coreFormation.getPrix()).isEqualTo(MONTESSORI_PRICE);
        assertThat(coreFormation.getPoints()).isNotNull().extracting(Point::getContent).containsExactly(LvlUpApplication.FIRST_PARAGRAPHE, LvlUpApplication.SECOND_PARAGRAPHE);
        assertThat(coreFormation.getPdf()).isNotNull();
        assertThat(coreFormation.getImage()).isNotNull();
    }

    @Test
    public void updateFormationPdfTest() throws Exception {
        this.formation.setId(formationRepository.findByNom(MONTESSORI_NAME).map(fr.jihadoussad.lvlup.core.entities.Formation::getId).orElseThrow());

        mvc.perform(multipart("/admin/formation")
                        .file("formation", this.objectMapper.writeValueAsString(this.formation).getBytes(StandardCharsets.UTF_8))
                        .file(pdf)
                        .with(request -> {request.setMethod(HttpMethod.PUT.name()); return request;})
                        .servletPath("/admin/formation")
                        .header("Access-Control-Request-Method", "PUT")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie))
                .andExpect(status().isAccepted());

        final fr.jihadoussad.lvlup.core.entities.Formation coreFormation = formationRepository.findByNom(MONTESSORI_NAME).orElse(null);

        assertThat(coreFormation).isNotNull();
        assertThat(coreFormation.getPrix()).isEqualTo(MONTESSORI_PRICE);
        assertThat(coreFormation.getPoints()).isNotNull().extracting(Point::getContent).containsExactly(LvlUpApplication.FIRST_PARAGRAPHE, LvlUpApplication.SECOND_PARAGRAPHE);
        assertThat(coreFormation.getPdf()).isNotNull();
        assertThat(coreFormation.getImage()).isNotNull();
    }

    @Test
    public void createFormationWhenUserNotAuthenticatedTest() throws Exception {
        mvc.perform(post("/admin/formation")
                        .servletPath("/admin/formation")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void createFormationWithOriginNotAllowedTest() throws Exception {
        mvc.perform(post("/admin/formation")
                        .servletPath("/admin/formation")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://lvlup-hacker.com")
                        .cookie(accessTokenCookie)
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void createFormationWithGetHttpMethodTest() throws Exception {
        mvc.perform(get("/admin/formation")
                        .servletPath("/admin/formation")
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie)
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void createFormationWithNoContentTest() throws Exception {
        mvc.perform(post("/admin/formation")
                        .servletPath("/admin/formation")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie)
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createFormationWithNoImageTest() throws Exception {
        mvc.perform(multipart("/admin/formation")
                        .file("formation", this.objectMapper.writeValueAsString(this.formation).getBytes(StandardCharsets.UTF_8))
                        .servletPath("/admin/formation")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createFormationWithNoPdfTest() throws Exception {
        mvc.perform(multipart("/admin/formation")
                        .file("formation", this.objectMapper.writeValueAsString(this.formation).getBytes(StandardCharsets.UTF_8))
                        .file(this.image)
                        .servletPath("/admin/formation")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deleteFormationWhenUserNotAuthenticatedTest() throws Exception {
        mvc.perform(delete("/admin/formation/" + 10)
                        .servletPath("/admin/formation/" + 10)
                        .header("Access-Control-Request-Method", "DELETE")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void deleteFormationWithOriginNotAllowedTest() throws Exception {
        mvc.perform(delete("/admin/formation/" + 10)
                        .servletPath("/admin/formation/" + 10)
                        .header("Access-Control-Request-Method", "DELETE")
                        .header("Origin", "https://lvlup-hacker.com")
                        .cookie(accessTokenCookie)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void deleteFormationWithPostHttpMethodTest() throws Exception {
        mvc.perform(post("/admin/formation/" + 10)
                        .servletPath("/admin/formation/" + 10)
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void deleteFormationWhenNotFoundTest() throws Exception {
        final String message =
                objectMapper.readValue(
                        mvc.perform(delete("/admin/formation/" + 10)
                        .servletPath("/admin/formation/" + 10)
                        .header("Access-Control-Request-Method", "DELETE")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8), String.class);

        assertThat(message).isEqualTo(ResponseCode.FORMATION_NOT_EXIST.message);
    }

    @Test
    public void createAndDeleteFormationTest() throws Exception {
        formation.setNom(NEW_FORMATION_NAME);
        formation.setPhrase(PHRASE);
        formation.setPoints(List.of(FIRST_PARAGRAPHE, SECOND_PARAGRAPHE));
        formation.setPrix(NEW_FORMATION_PRICE);

        final String redirectedUrl =
                mvc.perform(multipart("/admin/formation")
                        .file("formation", this.objectMapper.writeValueAsString(this.formation).getBytes(StandardCharsets.UTF_8))
                        .file(image)
                        .file(pdf)
                        .servletPath("/admin/formation")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie))
                .andExpect(status().isCreated()).andReturn().getResponse().getRedirectedUrl();

        assertThat(redirectedUrl).isNotNull();

        final Formation formation =
                objectMapper.readValue(
                        mvc.perform(get(redirectedUrl)
                                        .servletPath(redirectedUrl)
                                        .header("Access-Control-Request-Method", "GET")
                                        .header("Origin", "https://" + context.getFrontUrl())
                                        .cookie(accessTokenCookie)
                                        .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk())
                                .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8), Formation.class);

        assertThat(formation).isNotNull();
        assertThat(formation.getNom()).isEqualTo(this.formation.getNom());
        assertThat(formation.getPrix()).isEqualTo(this.formation.getPrix());
        assertThat(formation.getPhrase()).isEqualTo(this.formation.getPhrase());
        assertThat(formation.getPoints()).hasSize(2).containsExactly(FIRST_PARAGRAPHE, SECOND_PARAGRAPHE);
        assertThat(imageRepository.findById(formation.getImageId())).isNotEmpty();
        assertThat(pdfRepository.findById(formation.getPdfId())).isNotEmpty();

        mvc.perform(delete("/admin/formation/" + formation.getId())
                        .servletPath("/admin/formation/" + formation.getId())
                        .header("Access-Control-Request-Method", "DELETE")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted());

        assertThat(formationRepository.findById(formation.getId())).isEmpty();
        assertThat(imageRepository.findById(formation.getImageId())).isEmpty();
        assertThat(pdfRepository.findById(formation.getPdfId())).isEmpty();
    }

    @Test
    public void createSessionWhenUserNotAuthenticatedTest() throws Exception {
        mvc.perform(post("/admin/session")
                        .servletPath("/admin/session")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void createSessionWithOriginNotAllowedTest() throws Exception {
        mvc.perform(post("/admin/session")
                        .servletPath("/admin/session")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://lvlup-hacker.com")
                        .cookie(accessTokenCookie)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void createSessionWithGetHttpMethodTest() throws Exception {
        mvc.perform(get("/admin/session")
                        .servletPath("/admin/session")
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void createSessionWithNoContentTest() throws Exception {
        mvc.perform(post("/admin/session")
                        .servletPath("/admin/session")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createSessionWithFormationNotFoundTest() throws Exception {
        final Session session = new Session();
        final Formation formation = new Formation();
        formation.setId(10L);
        session.setFormation(formation);
        final String message =
                objectMapper.readValue(
                        mvc.perform(post("/admin/session")
                        .servletPath("/admin/session")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(session)))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8), String.class);

        assertThat(message).isEqualTo(ResponseCode.FORMATION_NOT_EXIST.message);
    }

    @Test
    public void deleteSessionWhenUerNotAuthenticatedTest() throws Exception {
        mvc.perform(delete("/admin/session/" + 10)
                        .servletPath("/admin/session/" + 10)
                        .header("Access-Control-Request-Method", "DELETE")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void deleteSessionWithOriginNotAllowedTest() throws Exception {
        mvc.perform(delete("/admin/session/" + 10)
                        .servletPath("/admin/session/" + 10)
                        .header("Access-Control-Request-Method", "DELETE")
                        .header("Origin", "https://lvlup-hacker.com")
                        .cookie(accessTokenCookie)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void deleteSessionWithPostHttpMethodTest() throws Exception {
        mvc.perform(post("/admin/session/" + 10)
                        .servletPath("/admin/session/" + 10)
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void deleteSessionWhenNotFoundTest() throws Exception {
        final String message =
                objectMapper.readValue(
                        mvc.perform(delete("/admin/session/" + 10)
                                        .servletPath("/admin/session/" + 10)
                                        .header("Access-Control-Request-Method", "DELETE")
                                        .header("Origin", "https://" + context.getFrontUrl())
                                        .cookie(accessTokenCookie)
                                        .contentType(MediaType.APPLICATION_JSON)
                                        .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8), String.class);

        assertThat(message).isEqualTo(ResponseCode.SESSION_NOT_EXIST.message);
    }

    @Test
    @Transactional
    public void createAndDeleteSessionTest() throws Exception {
        final Session createdSession = new Session();
        createdSession.setDate(LocalDate.now().plusMonths(3));
        createdSession.setLieu("New York");
        final Formation formation = new Formation();
        formation.setId(formationRepository.findByNom(MONTESSORI_NAME).map(fr.jihadoussad.lvlup.core.entities.Formation::getId).orElseThrow());
        createdSession.setFormation(formation);
        final String redirectedUrl =
                mvc.perform(post("/admin/session")
                                .servletPath("/admin/session")
                                .header("Access-Control-Request-Method", "POST")
                                .header("Origin", "https://" + context.getFrontUrl())
                                .cookie(accessTokenCookie)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(createdSession)))
                        .andExpect(status().isCreated()).andReturn().getResponse().getRedirectedUrl();

        assertThat(redirectedUrl).isNotNull();

        final Session session =
                objectMapper.readValue(
                        mvc.perform(get(redirectedUrl)
                                        .servletPath(redirectedUrl)
                                        .header("Access-Control-Request-Method", "GET")
                                        .header("Origin", "https://" + context.getFrontUrl())
                                        .cookie(accessTokenCookie)
                                        .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk())
                                .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8), Session.class);

        assertThat(session).isNotNull();
        assertThat(session.getDate()).isEqualTo(createdSession.getDate());
        assertThat(session.getLieu()).isEqualTo(createdSession.getLieu());
        assertThat(session.getFormation().getNom()).isEqualTo(MONTESSORI_NAME);

        mvc.perform(delete("/admin/session/" + session.getId())
                        .servletPath("/admin/session/" + session.getId())
                        .header("Access-Control-Request-Method", "DELETE")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .cookie(accessTokenCookie)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted());

        assertThat(sessionRepository.findById(session.getId())).isEmpty();
        final fr.jihadoussad.lvlup.core.entities.Formation coreFormation = formationRepository.findByNom(MONTESSORI_NAME).orElse(null);
        assertThat(coreFormation).isNotNull();
        assertThat(coreFormation.getImage()).isNotNull();
        assertThat(coreFormation.getPdf()).isNotNull();
        assertThat(coreFormation.getPoints()).extracting(Point::getContent)
                .containsExactly(LvlUpApplication.FIRST_PARAGRAPHE, LvlUpApplication.SECOND_PARAGRAPHE);
    }

}
