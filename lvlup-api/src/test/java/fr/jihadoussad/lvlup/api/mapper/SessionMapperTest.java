package fr.jihadoussad.lvlup.api.mapper;

import fr.jihadoussad.lvlup.core.entities.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class SessionMapperTest {

    @Autowired
    private SessionMapper sessionMapper;

    @Test
    public void coreToContractTest() {
        final Session coreSession = new Session();
        coreSession.setId(1L);
        coreSession.setLieu("lieu");
        coreSession.setDate(LocalDate.now());
        final Formation coreFormation = new Formation();
        coreFormation.setNom("formation");
        coreFormation.setPrix(1000);
        coreFormation.setImage(new Image());
        coreFormation.setPdf(new Pdf());
        final Point point = new Point();
        point.setContent("content");
        coreFormation.addPoint(point);
        coreSession.setFormation(coreFormation);

        final fr.jihadoussad.lvlup.contract.Session contractSession = sessionMapper.coreToContract(coreSession);

        assertThat(contractSession).isNotNull();
        assertThat(contractSession.getId()).isEqualTo(coreSession.getId());
        assertThat(contractSession.getDate()).isEqualTo(coreSession.getDate());
        assertThat(contractSession.getLieu()).isEqualTo(coreSession.getLieu());
        final fr.jihadoussad.lvlup.contract.Formation contractFormation = contractSession.getFormation();
        assertThat(contractFormation).isNotNull();
        assertThat(contractFormation.getId()).isEqualTo(coreFormation.getId());
        assertThat(contractFormation.getNom()).isEqualTo(coreFormation.getNom());
        assertThat(contractFormation.getPrix()).isEqualTo(coreFormation.getPrix());
        assertThat(contractFormation.getPoints()).isNotNull().hasSize(1)
                .containsExactly(point.getContent());
        assertThat(contractFormation.getImageId()).isNull();
        assertThat(contractFormation.getPdfId()).isNull();
    }
}
