package fr.jihadoussad.lvlup.api.controller;

import fr.jihadoussad.lvlup.contract.ContractValidationException;
import fr.jihadoussad.lvlup.contract.Registration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.mockito.ArgumentMatchers.anyString;

@SpringBootTest
public class RegistrationControllerTest {

    @MockBean
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private RegistrationController registrationController;

    @BeforeEach
    public void setUp() {
        Mockito.doNothing().when(rabbitTemplate).convertAndSend(anyString(), anyString());
    }

    @Test
    public void sendMailRegistrationTest() throws ContractValidationException {
        final Registration registration = new Registration();
        registration.setFormation("formation");
        registration.setSessionDate("décembre 2021");
        registration.setLastname("lastname");
        registration.setFirstname("firstname");
        registration.setEmail("test@test.fr");
        registration.setPhoneNumber("0707070707");

        registrationController.sendMailRegistration(registration);
    }
}
