package fr.jihadoussad.lvlup.api.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.lvlup.api.context.LvlupApiContext;
import fr.jihadoussad.lvlup.contract.Formation;
import fr.jihadoussad.lvlup.contract.ResponseCode;
import fr.jihadoussad.lvlup.contract.Session;
import fr.jihadoussad.lvlup.core.entities.User;
import fr.jihadoussad.lvlup.core.repositories.ImageRepository;
import fr.jihadoussad.lvlup.core.repositories.PdfRepository;
import fr.jihadoussad.lvlup.core.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

import static fr.jihadoussad.lvlup.api.LvlUpApplication.*;
import static fr.jihadoussad.lvlup.contract.ResponseCode.ACCOUNT_BLOCKED;
import static fr.jihadoussad.lvlup.contract.ResponseCode.EMAIL_NOT_FOUND;
import static fr.jihadoussad.tools.api.response.ResponseCode.INVALID_PASSWORD;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class VisitorControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private LvlupApiContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @Resource
    private ImageRepository imageRepository;

    @Resource
    private PdfRepository pdfRepository;

    @Resource
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void getFormationsWithOriginNotAllowedTest() throws Exception {
        mvc.perform(get("/formations")
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://lvlup-hacker.com")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void getFormationsWithPostHttpMethodTest() throws Exception {
        mvc.perform(post("/formations")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void getFormationByIdWithOriginNotAllowedTest() throws Exception {
        mvc.perform(get("/formation/1")
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://lvlup-hacker.com")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void getFormationByIdWithPostHttpMethodTest() throws Exception {
        mvc.perform(post("/formation/1")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void getFormationByIdWhenNotFoundTest() throws Exception {
        final String message =
                objectMapper.readValue(
                        mvc.perform(get("/formation/10")
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8), String.class);

        assertThat(message).isEqualTo(ResponseCode.FORMATION_NOT_EXIST.message);
    }

    @Test
    public void getFormationTest() throws Exception {
        final List<Formation> formations =
                objectMapper.readValue(
                        mvc.perform(get("/formations")
                                        .header("Access-Control-Request-Method", "GET")
                                        .header("Origin", "https://" + context.getFrontUrl())
                                        .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk())
                                .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8), new TypeReference<List<Formation>>() {});

        assertThat(formations)
                .isNotNull()
                .hasSize(2)
                .extracting(Formation::getNom).containsExactly(MONTESSORI_NAME, CREATION_ENTREPRISE_NAME);

        final Formation formation =
                objectMapper.readValue(
                        mvc.perform(get("/formation/" + formations.get(0).getId())
                                        .header("Access-Control-Request-Method", "GET")
                                        .header("Origin", "https://" + context.getFrontUrl())
                                        .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk())
                                .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8), Formation.class);

        assertThat(formation).isNotNull();
        assertThat(formation.getNom()).isEqualTo(formations.get(0).getNom());
        assertThat(formation.getPhrase()).isEqualTo(formations.get(0).getPhrase());
        assertThat(formation.getPrix()).isEqualTo(formations.get(0).getPrix());
        assertThat(formation.getPoints()).isNotNull().hasSize(2);
    }

    @Test
    public void getImageByIdWithOriginNotAllowedTest() throws Exception {
        mvc.perform(get("/image/1")
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://lvlup-hacker.com")
                        .accept(MediaType.IMAGE_JPEG))
                .andExpect(status().isForbidden());
    }

    @Test
    public void getImageByIdWithPostHttpMethodTest() throws Exception {
        mvc.perform(post("/image/1")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .accept(MediaType.IMAGE_JPEG))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void getImageByIdWhenNotFoundTest() throws Exception {
        final String message =
                objectMapper.readValue(mvc.perform(get("/image/10")
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .accept(MediaType.IMAGE_JPEG))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8), String.class);

        assertThat(message).isEqualTo(ResponseCode.IMAGE_NOT_EXIST.message);
    }

    @Test
    public void getImageByIdTest() throws Exception {
        final Long id = imageRepository.findAll().get(0).getId();
        mvc.perform(get("/image/" + id)
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .accept(MediaType.IMAGE_JPEG))
                .andExpect(status().isOk());
    }

    @Test
    public void getPdfByIdWithOriginNotAllowedTest() throws Exception {
        mvc.perform(get("/pdf/1")
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://lvlup-hacker.com")
                        .accept(MediaType.APPLICATION_OCTET_STREAM))
                .andExpect(status().isForbidden());
    }

    @Test
    public void getPdfByIdWithPostHttpMethodTest() throws Exception {
        mvc.perform(post("/pdf/1")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .accept(MediaType.APPLICATION_OCTET_STREAM))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void getPdfByIdWhenNotFoundTest() throws Exception {
        final String message =
                objectMapper.readValue(mvc.perform(get("/pdf/100")
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .accept(MediaType.APPLICATION_PDF))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8), String.class);

        assertThat(message).isEqualTo(ResponseCode.PDF_NOT_EXIST.message);
    }

    @Test
    public void getPdfByIdTest() throws Exception {
        final Long id = pdfRepository.findAll().get(0).getId();
        mvc.perform(get("/pdf/" + id)
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .accept(MediaType.APPLICATION_PDF))
                .andExpect(status().isOk());
    }

    @Test
    public void getSessionsWithOriginNotAllowedTest() throws Exception {
        mvc.perform(get("/sessions")
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://lvlup-hacker.com")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void getSessionsWithPostHttpMethodTest() throws Exception {
        mvc.perform(post("/sessions")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void getSessionByIdWithOriginNotAllowedTest() throws Exception {
        mvc.perform(get("/session/1")
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://lvlup-hacker.com")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void getSessionByIdWithPostHttpMethodTest() throws Exception {
        mvc.perform(post("/session/1")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void getSessionByIdWhenNotFoundTest() throws Exception {
        final String message =
                objectMapper.readValue(mvc.perform(get("/session/10")
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8), String.class);

        assertThat(message).isEqualTo(ResponseCode.SESSION_NOT_EXIST.message);
    }

    @Test
    public void getSessionTest() throws Exception {
        final List<Session> sessions =
                objectMapper.readValue(
                        mvc.perform(get("/sessions")
                                        .header("Access-Control-Request-Method", "GET")
                                        .header("Origin", "https://" + context.getFrontUrl())
                                        .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk())
                                .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8), new TypeReference<List<Session>>() {});

        assertThat(sessions).isNotNull().hasSize(2);

        final Session session =
                objectMapper.readValue(
                        mvc.perform(get("/session/" + sessions.get(0).getId())
                                        .header("Access-Control-Request-Method", "GET")
                                        .header("Origin", "https://" + context.getFrontUrl())
                                        .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk())
                                .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8), Session.class);

        assertThat(session).isNotNull();
        assertThat(session.getDate()).isEqualTo(sessions.get(0).getDate().toString());
        assertThat(session.getLieu()).isEqualTo(sessions.get(0).getLieu());
    }

    @Test
    public void resetPasswordWithOriginNotAllowedTest() throws Exception {
        mvc.perform(post("/password/reset")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://lvlup-hacker.com")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void resetPasswordWithGetHttpMethodTest() throws Exception {
        mvc.perform(get("/password/reset")
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void resetPasswordWithNoEmailTest() throws Exception {
        mvc.perform(post("/password/reset")
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void resetPasswordWhenEmailNotFoundTest() throws Exception {
        final String message =
                objectMapper.readValue(
                        mvc.perform(post("/password/reset")
                                        .param("email", "emailNotFound")
                                        .header("Access-Control-Request-Method", "POST")
                                        .header("Origin", "https://" + context.getFrontUrl())
                                        .contentType(MediaType.APPLICATION_JSON)
                                        .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8), String.class);

        assertThat(message).isEqualTo(EMAIL_NOT_FOUND.message);
    }

    @Test
    public void resetPasswordWhenAccountBlockedTest() throws Exception {
        userRepository.findByLogin(USERNAME).ifPresent(user -> {
            user.setAuthenticationRetry(context.getMaxRetry());
            userRepository.saveAndFlush(user);
        });

        final String message =
                objectMapper.readValue(
                        mvc.perform(post("/password/reset")
                                        .param("email", EMAIL)
                                        .header("Access-Control-Request-Method", "POST")
                                        .header("Origin", "https://" + context.getFrontUrl())
                                        .contentType(MediaType.APPLICATION_JSON)
                                        .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isForbidden()).andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8), String.class);

        assertThat(message).isEqualTo(ACCOUNT_BLOCKED.message);

        userRepository.findByLogin(USERNAME).ifPresent(user -> {
            user.setAuthenticationRetry(0);
            userRepository.saveAndFlush(user);
        });
    }

    @Test
    public void setPasswordWhenAMQPDownTest() throws Exception {
        mvc.perform(post("/password/reset")
                        .param("email", EMAIL)
                        .header("Access-Control-Request-Method", "POST")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void setPasswordWithOriginNotAllowedTest() throws Exception {
        mvc.perform(put("/password")
                        .header("Access-Control-Request-Method", "PUT")
                        .header("Origin", "https://lvlup-hacker.com")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void setPasswordWithGetHttpMethodTest() throws Exception {
        mvc.perform(get("/password")
                        .header("Access-Control-Request-Method", "GET")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void setPasswordWithNoPasswordTest() throws Exception {
        mvc.perform(put("/password")
                        .header("Access-Control-Request-Method", "PUT")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void setPasswordWithNoTokenTest() throws Exception {
        mvc.perform(put("/password")
                        .param("new", "newPassword")
                        .header("Access-Control-Request-Method", "PUT")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void setPasswordWhenPasswordInvalidTest() throws Exception {
        final String message =
                objectMapper.readValue(
                        mvc.perform(put("/password")
                                        .param("new", "password")
                                        .param("token", "token")
                                        .header("Access-Control-Request-Method", "PUT")
                                        .header("Origin", "https://" + context.getFrontUrl())
                                        .contentType(MediaType.APPLICATION_JSON)
                                        .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8), String.class);

        assertThat(message).isEqualTo(INVALID_PASSWORD.message);
    }

    @Test
    public void setPasswordWhenTokenInvalidTest() throws Exception {
        mvc.perform(put("/password")
                        .param("new", "p@ssword12345")
                        .param("token", "token")
                        .header("Access-Control-Request-Method", "PUT")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void setPasswordTest() throws Exception {
        userRepository.findByLogin(USERNAME).ifPresent(user -> {
            user.setAuthenticationRetry(1);
            userRepository.saveAndFlush(user);
        });

        mvc.perform(put("/password")
                        .param("new", "p@ssword12345")
                        .param("token", RESET_PASSWORD_TOKEN)
                        .header("Access-Control-Request-Method", "PUT")
                        .header("Origin", "https://" + context.getFrontUrl())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        final Optional<User> user = userRepository.findByLogin(USERNAME);
        assertThat(user).isNotEmpty().get().extracting(User::getAuthenticationRetry).isEqualTo(0);
        assertThat(passwordEncoder.matches("p@ssword12345", user.get().getPassword())).isTrue();

        userRepository.findByLogin(USERNAME).ifPresent(u -> {
            u.setPassword(passwordEncoder.encode(PASSWORD));
            userRepository.saveAndFlush(u);
        });
    }
}
