package fr.jihadoussad.lvlup.api.controller;

import fr.jihadoussad.lvlup.contract.ContractValidationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import static fr.jihadoussad.lvlup.api.LvlUpApplication.EMAIL;
import static org.mockito.ArgumentMatchers.anyString;

@SpringBootTest
@ActiveProfiles("test")
public class VisitorControllerTest {

    @MockBean
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private VisitorController visitorController;

    @BeforeEach
    public void setUp() {
        Mockito.doNothing().when(rabbitTemplate).convertAndSend(anyString(), anyString());
    }

    @Test
    public void resetPasswordTest() throws ContractValidationException {
        visitorController.resetPassword(EMAIL);
    }
}
