package fr.jihadoussad.lvlup.api.security;

import fr.jihadoussad.lvlup.api.context.LvlupApiContext;
import fr.jihadoussad.lvlup.core.entities.Role;
import fr.jihadoussad.lvlup.core.entities.User;
import fr.jihadoussad.lvlup.core.repositories.UserRepository;
import fr.jihadoussad.tools.api.response.ResponseCode;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserService implements UserDetailsService {

    @Resource
    private UserRepository userRepository;

    private final LvlupApiContext context;

    public UserService(final LvlupApiContext context) {
        this.context = context;
    }

    public void unsuccessfulAuthentication(final String username) {
        userRepository.findByLogin(username).ifPresent(user -> {
            user.setAuthenticationRetry(user.getAuthenticationRetry() + 1);
            userRepository.saveAndFlush(user);
        });
    }

    public void clearMaxRetry(final String username) {
        userRepository.findByLogin(username).ifPresent(user -> {
            user.setAuthenticationRetry(0);
            userRepository.saveAndFlush(user);
        });
    }

    public boolean maxRetryAuthentication(final String username) {
        return userRepository.findByLogin(username).map(user -> user.getAuthenticationRetry() >= context.getMaxRetry()).orElse(false);
    }

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        final User user = userRepository.findByLogin(username).orElseThrow(() -> new UsernameNotFoundException(ResponseCode.USER_NOT_FOUND.message));
        final Collection<SimpleGrantedAuthority> authorities = user.getRoles().stream().map(Role::getAuthority)
                .map(SimpleGrantedAuthority::new).collect(Collectors.toList());

        return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), authorities);
    }
}
