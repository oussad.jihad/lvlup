package fr.jihadoussad.lvlup.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.lvlup.amqp.config.AmqpContext;
import fr.jihadoussad.lvlup.api.context.LvlupApiContext;
import fr.jihadoussad.lvlup.api.mapper.FormationMapper;
import fr.jihadoussad.lvlup.api.mapper.SessionMapper;
import fr.jihadoussad.lvlup.contract.ContractValidationException;
import fr.jihadoussad.lvlup.contract.Formation;
import fr.jihadoussad.lvlup.contract.Session;
import fr.jihadoussad.lvlup.core.entities.Image;
import fr.jihadoussad.lvlup.core.entities.Pdf;
import fr.jihadoussad.lvlup.core.entities.User;
import fr.jihadoussad.lvlup.core.repositories.*;
import fr.jihadoussad.tokenserver.contract.TokenService;
import fr.jihadoussad.tokenserver.contract.input.Token;
import fr.jihadoussad.tokenserver.contract.input.VerifyTokenInput;
import fr.jihadoussad.tools.api.response.ResponseCode;
import fr.jihadoussad.tools.validators.InputValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

import static fr.jihadoussad.lvlup.contract.ResponseCode.*;
import static fr.jihadoussad.tools.validators.CommonRegexValidator.PASSWORD;

@RestController
public class VisitorController {

    private final Logger logger = LoggerFactory.getLogger(VisitorController.class);

    @Resource
    private FormationRepository formationRepository;

    @Resource
    private ImageRepository imageRepository;

    @Resource
    private PdfRepository pdfRepository;

    @Resource
    private SessionRepository sessionRepository;

    @Resource
    private UserRepository userRepository;

    private final AmqpContext amqpContext;

    private final LvlupApiContext lvlupApiContext;

    private final FormationMapper formationMapper;

    private final ObjectMapper objectMapper;

    private final SessionMapper sessionMapper;

    private final PasswordEncoder passwordEncoder;

    private final RabbitTemplate rabbitTemplate;

    private final TokenService tokenService;

    public VisitorController(final AmqpContext amqpContext, final LvlupApiContext lvlupApiContext,
                             final FormationMapper formationMapper, ObjectMapper objectMapper, final SessionMapper sessionMapper,
                             final PasswordEncoder passwordEncoder, final RabbitTemplate rabbitTemplate,
                             final TokenService tokenService) {
        this.amqpContext = amqpContext;
        this.lvlupApiContext = lvlupApiContext;
        this.formationMapper = formationMapper;
        this.objectMapper = objectMapper;
        this.sessionMapper = sessionMapper;
        this.passwordEncoder = passwordEncoder;
        this.rabbitTemplate = rabbitTemplate;
        this.tokenService = tokenService;
    }

    @GetMapping(value = "formations", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Formation>> getFormations() {
        logger.info("BEGIN - Try to find formations store in database");
        final List<Formation> formations = formationRepository.findAll().stream()
                .map(formationMapper::coreToContract).collect(Collectors.toList());
        logger.info("END - Found {} formation(s)", formations.size());

        return ResponseEntity.ok(formations);
    }

    @GetMapping(value = "formation/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Formation> getFormation(@PathVariable final Long id) throws ContractValidationException {
        logger.info("BEGIN - Try to find formation store in database with id = {}", id);
        final fr.jihadoussad.lvlup.core.entities.Formation formation = formationRepository.findById(id)
                .orElseThrow(() -> new ContractValidationException(FORMATION_NOT_EXIST.message, FORMATION_NOT_EXIST.code, HttpStatus.NOT_FOUND.value()));
        logger.info("END - Formation found");

        return ResponseEntity.ok(formationMapper.coreToContract(formation));
    }

    @GetMapping(value = "image/{id}", produces = { MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE })
    public byte[] getImage(@PathVariable final Long id) throws ContractValidationException {
        logger.info("BEGIN - Try to find image store in database with id = {}", id);
        final Image image = imageRepository.findById(id)
                .orElseThrow(() -> new ContractValidationException(IMAGE_NOT_EXIST.message, IMAGE_NOT_EXIST.code, HttpStatus.NOT_FOUND.value()));
        logger.info("END - Image found");

        return image.getFile();
    }

    @GetMapping(value = "pdf/{id}", produces = MediaType.APPLICATION_PDF_VALUE)
    public byte[] getPdf(@PathVariable final Long id) throws ContractValidationException {
        logger.info("BEGIN - Try to find pdf file store in database with id = {}", id);
        final Pdf pdf = pdfRepository.findById(id)
                .orElseThrow(() -> new ContractValidationException(PDF_NOT_EXIST.message, PDF_NOT_EXIST.code, HttpStatus.NOT_FOUND.value()));
        logger.info("END - Pdf file found");

        return pdf.getFile();
    }

    @GetMapping(value = "sessions", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Session>> getSessionsAvailable() {
        logger.info("BEGIN - Try to find sessions store in database");
        final List<Session> sessions = sessionRepository.getSessionAvailable()
                .orElse(List.of())
                .stream().map(sessionMapper::coreToContract).collect(Collectors.toList());
        logger.info("END - Found {} session(s) available", sessions.size());

        return ResponseEntity.ok(sessions);
    }

    @GetMapping(value = "session/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Session> getSession(@PathVariable final Long id) throws ContractValidationException {
        logger.info("BEGIN - Try to find session store in database with id = {}", id);
        final fr.jihadoussad.lvlup.core.entities.Session session = sessionRepository.findById(id)
                .orElseThrow(() -> new ContractValidationException(SESSION_NOT_EXIST.message, SESSION_NOT_EXIST.code, HttpStatus.NOT_FOUND.value()));
        logger.info("END - Session found");

        return ResponseEntity.ok(sessionMapper.coreToContract(session));
    }

    @PostMapping(value = "password/reset", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> resetPassword(@RequestParam("email") final String email) throws ContractValidationException {
        final int authenticationRetry = userRepository.findByEmail(email)
                .map(User::getAuthenticationRetry)
                .orElseThrow(() -> new ContractValidationException(EMAIL_NOT_FOUND.message, EMAIL_NOT_FOUND.code, HttpStatus.NOT_FOUND.value()));

        if (authenticationRetry >= lvlupApiContext.getMaxRetry()) {
            throw new ContractValidationException(ACCOUNT_BLOCKED.message, ACCOUNT_BLOCKED.code, HttpStatus.FORBIDDEN.value());
        }

        try {
            rabbitTemplate.convertAndSend(amqpContext.getPasswordQueueName(), objectMapper.writeValueAsString(email));
            logger.info("New reset password account request type message has sent to the queue {}", amqpContext.getPasswordQueueName());
            return ResponseEntity.ok().build();
        } catch (final Exception e) {
            logger.error(e.toString());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping(value = "password", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> setPassword(@RequestParam("new") final String password, @RequestParam("token") final String token) throws ContractValidationException {
        logger.info("BEGIN - Try to set user password account");
        if (!InputValidator.getInstance(PASSWORD.pattern).validate(password)) {
            throw new ContractValidationException(ResponseCode.INVALID_PASSWORD.message, ResponseCode.INVALID_PASSWORD.code);
        }

        try {
            final VerifyTokenInput input = new VerifyTokenInput();
            input.setToken(token);
            input.setSecretKey(amqpContext.getPasswordSecretKey());
            logger.debug("Call token service to verify token with following input: {}", input);
            userRepository.findByEmail(tokenService.verifyToken(input).getSubject())
                    .ifPresent(user -> {
                        user.setAuthenticationRetry(0);
                        user.setPassword(passwordEncoder.encode(password));
                        userRepository.saveAndFlush(user);
                    });
            final Token jwt = new Token();
            jwt.setToken(token);
            logger.debug("Call token service to invalidate password token...");
            tokenService.invalidateToken(jwt);
            logger.info("END - User password account successfully changed.");
        } catch (final fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException e) {
            throw new ContractValidationException(e.getMessage(), e.getCode(), HttpStatus.FORBIDDEN.value());
        }

        return ResponseEntity.ok().build();
    }
}
