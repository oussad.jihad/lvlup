package fr.jihadoussad.lvlup.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.lvlup.api.mapper.FormationMapper;
import fr.jihadoussad.lvlup.api.mapper.SessionMapper;
import fr.jihadoussad.lvlup.contract.ContractValidationException;
import fr.jihadoussad.lvlup.contract.Formation;
import fr.jihadoussad.lvlup.contract.Session;
import fr.jihadoussad.lvlup.core.entities.Image;
import fr.jihadoussad.lvlup.core.entities.Pdf;
import fr.jihadoussad.lvlup.core.entities.Point;
import fr.jihadoussad.lvlup.core.repositories.FormationRepository;
import fr.jihadoussad.lvlup.core.repositories.SessionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Optional;

import static fr.jihadoussad.lvlup.contract.ResponseCode.FORMATION_NOT_EXIST;
import static fr.jihadoussad.lvlup.contract.ResponseCode.SESSION_NOT_EXIST;

@RestController
@RequestMapping("admin")
public class AdminController {

    private final Logger logger = LoggerFactory.getLogger(AdminController.class);

    @Resource
    private FormationRepository formationRepository;

    @Resource
    private SessionRepository sessionRepository;

    private final FormationMapper formationMapper;

    private final SessionMapper sessionMapper;

    private final ObjectMapper objectMapper;

    public AdminController(final FormationMapper formationMapper, final SessionMapper sessionMapper, final ObjectMapper objectMapper) {
        this.formationMapper = formationMapper;
        this.sessionMapper = sessionMapper;
        this.objectMapper = objectMapper;
    }

    @PostMapping(value = "/formation", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Long> createFormation(@RequestPart("formation") final MultipartFile jsonFormation,
                                                @RequestPart("image") final MultipartFile imageUploaded,
                                                @RequestPart("pdf") final MultipartFile pdfUploaded) throws IOException {
        final Formation formation = objectMapper.readValue(jsonFormation.getBytes(), Formation.class);
        logger.info("BEGIN - Try to create new formation with following input: {}", formation.toString());
        final Image image = new Image();
        image.setFile(imageUploaded.getBytes());
        logger.debug("New image built");
        final Pdf pdf = new Pdf();
        pdf.setFile(pdfUploaded.getBytes());
        logger.debug("new pdf built");
        final Long id = formationRepository.saveAndFlush(formationMapper.contractToCore(formation, image, pdf)).getId();
        logger.info("END - Formation has successfully created with id={}", id);

        return ResponseEntity.created(ServletUriComponentsBuilder.fromPath("/formation").path("/{id}")
                .buildAndExpand(id).toUri()).body(id);
    }

    @PutMapping(value = "/formation", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> updateFormation(@RequestPart(value = "formation") final MultipartFile jsonFormation,
                                                @RequestPart(value = "image", required = false) final MultipartFile imageUploaded,
                                                @RequestPart(value = "pdf", required = false) final MultipartFile pdfUploaded) throws ContractValidationException, IOException {
        final Formation contractFormation = objectMapper.readValue(jsonFormation.getBytes(), Formation.class);
        logger.info("BEGIN - Try to update formation with following input: {}", contractFormation.toString());
        final fr.jihadoussad.lvlup.core.entities.Formation coreFormation = formationRepository.findById(contractFormation.getId())
                .orElseThrow(() -> new ContractValidationException(FORMATION_NOT_EXIST.message, FORMATION_NOT_EXIST.code, HttpStatus.NOT_FOUND.value()));
        logger.debug("Formation found in database: {}", coreFormation);

        logger.debug("Core formation setting...");
        Optional.ofNullable(contractFormation.getNom()).ifPresent(coreFormation::setNom);
        Optional.ofNullable(contractFormation.getPrix()).ifPresent(coreFormation::setPrix);
        Optional.ofNullable(contractFormation.getPoints()).ifPresent(points -> {
            coreFormation.getPoints().clear();
            points.forEach(content -> {
                final Point point = new Point();
                point.setContent(content);
                coreFormation.addPoint(point);
            });
        });
        if (imageUploaded != null) {
            final Image image = new Image();
            image.setFile(imageUploaded.getBytes());
            coreFormation.setImage(image);
        }
        if (pdfUploaded != null) {
            final Pdf pdf = new Pdf();
            pdf.setFile(pdfUploaded.getBytes());
            coreFormation.setPdf(pdf);
        }
        formationRepository.saveAndFlush(coreFormation);
        logger.debug("Formation stored in database: {}", coreFormation);
        logger.info("END - Formation has been updated");

        return ResponseEntity.accepted().build();
    }

    @DeleteMapping(value = "/formation/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deleteFormation(@PathVariable final Long id) throws ContractValidationException {
        logger.info("BEGIN - Try to delete formation with following id: {}", id);
        final fr.jihadoussad.lvlup.core.entities.Formation formation = formationRepository.findById(id)
                .orElseThrow(() -> new ContractValidationException(FORMATION_NOT_EXIST.message, FORMATION_NOT_EXIST.code, HttpStatus.NOT_FOUND.value()));
        logger.debug("Formation found in database: {}", formation);
        formationRepository.delete(formation);
        logger.info("END - Formation has successfully deleted");

        return ResponseEntity.accepted().build();
    }

    @PostMapping(value = "/session", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Long> createSession(@RequestBody final Session session) throws ContractValidationException {
        logger.info("BEGIN - Try to create new session with following input: {}", session.toString());
        final fr.jihadoussad.lvlup.core.entities.Formation coreFormation = formationRepository.findById(session.getFormation().getId())
                .orElseThrow(() -> new ContractValidationException(FORMATION_NOT_EXIST.message, FORMATION_NOT_EXIST.code, HttpStatus.NOT_FOUND.value()));
        logger.debug("Session formation found in database: {}", coreFormation);
        final fr.jihadoussad.lvlup.core.entities.Session coreSession = sessionMapper.contractToCore(session);
        coreSession.setFormation(coreFormation);
        final Long id = sessionRepository.saveAndFlush(coreSession).getId();
        logger.debug("Session stored in database: {}", coreSession);
        logger.info("END - Session has successfully created with id={}", id);

        return ResponseEntity.created(ServletUriComponentsBuilder.fromPath("/session").path("/{id}")
                .buildAndExpand(id).toUri()).body(id);
    }

    @DeleteMapping(value = "/session/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deleteSession(@PathVariable final Long id) throws ContractValidationException {
        logger.info("BEGIN - Try to delete session with following id: {}", id);
        final fr.jihadoussad.lvlup.core.entities.Session session = sessionRepository.findById(id)
                .orElseThrow(() -> new ContractValidationException(SESSION_NOT_EXIST.message, SESSION_NOT_EXIST.code, HttpStatus.NOT_FOUND.value()));
        logger.debug("Session found in database: {}", session);
        sessionRepository.delete(session);
        logger.info("END - Session has successfully deleted");

        return ResponseEntity.accepted().build();
    }
}
