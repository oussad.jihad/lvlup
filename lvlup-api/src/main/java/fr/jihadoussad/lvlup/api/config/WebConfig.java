package fr.jihadoussad.lvlup.api.config;

import fr.jihadoussad.lvlup.api.context.LvlupApiContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    private final String frontUrl;

    private final MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter;

    public WebConfig(final LvlupApiContext context, final MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter) {
        this.frontUrl = context.getFrontUrl();
        this.mappingJackson2HttpMessageConverter = mappingJackson2HttpMessageConverter;
    }

    @Override
    public void addCorsMappings(final CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("http://" + frontUrl, "https://" + frontUrl, "http://www." + frontUrl, "https://www." + frontUrl);
    }

    @Override
    public void configureMessageConverters(final List<HttpMessageConverter<?>> converters) {
        converters.add(mappingJackson2HttpMessageConverter);
        converters.add(byteArrayHttpMessageConverter());
    }

    @Bean
    ByteArrayHttpMessageConverter byteArrayHttpMessageConverter() {
        final ByteArrayHttpMessageConverter arrayHttpMessageConverter = new ByteArrayHttpMessageConverter();
        arrayHttpMessageConverter.setSupportedMediaTypes(List.of(MediaType.IMAGE_JPEG, MediaType.APPLICATION_PDF, MediaType.IMAGE_PNG, MediaType.APPLICATION_OCTET_STREAM));
        return arrayHttpMessageConverter;
    }
}
