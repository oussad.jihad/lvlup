package fr.jihadoussad.lvlup.api.context;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "lvlup-api")
public class LvlupApiContext {

    private String accessSecretKey;

    private final String accessTokenCookieName;

    private Long accessTokenExpiration;

    private final String authoritiesName;

    private String baseResourceUrl;

    private String frontUrl;

    private int maxRetry;

    private String subjectName;

    private List<String> webUrlIgnoring;

    public LvlupApiContext() {
        this.authoritiesName = "authorities";
        this.accessTokenCookieName = "X-LVLUP-ACCESS-TOKEN";
        this.accessTokenExpiration = 3600000L; // 1 hour
        this.maxRetry = 5;
        this.subjectName = "lvlup";
        this.webUrlIgnoring = List.of();
    }

    public String getAuthoritiesName() {
        return authoritiesName;
    }

    public String getAccessSecretKey() {
        return accessSecretKey;
    }

    public void setAccessSecretKey(final String accessSecretKey) {
        this.accessSecretKey = accessSecretKey;
    }

    public String getAccessTokenCookieName() {
        return accessTokenCookieName;
    }

    public String getBaseResourceUrl() {
        return baseResourceUrl;
    }

    public void setBaseResourceUrl(final String baseResourceUrl) {
        this.baseResourceUrl = baseResourceUrl;
    }

    public Long getAccessTokenExpiration() {
        return accessTokenExpiration;
    }

    public void setAccessTokenExpiration(final Long accessTokenExpiration) {
        this.accessTokenExpiration = accessTokenExpiration;
    }

    public String getFrontUrl() {
        return frontUrl;
    }

    public void setFrontUrl(final String frontUrl) {
        this.frontUrl = frontUrl;
    }

    public int getMaxRetry() {
        return maxRetry;
    }

    public void setMaxRetry(final int maxRetry) {
        this.maxRetry = maxRetry;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(final String subjectName) {
        this.subjectName = subjectName;
    }

    public List<String> getWebUrlIgnoring() {
        return webUrlIgnoring;
    }

    public void setWebUrlIgnoring(final List<String> webUrlIgnoring) {
        this.webUrlIgnoring = webUrlIgnoring;
    }
}
