package fr.jihadoussad.lvlup.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.lvlup.amqp.config.AmqpContext;
import fr.jihadoussad.lvlup.contract.ContractValidationException;
import fr.jihadoussad.lvlup.contract.Registration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegistrationController {

    private final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

    private final ObjectMapper objectMapper;
    private final String queueName;
    private final RabbitTemplate rabbitTemplate;

    public RegistrationController(final ObjectMapper objectMapper, final RabbitTemplate rabbitTemplate, final AmqpContext context) {
        this.objectMapper = objectMapper;
        this.queueName = context.getRegistrationQueueName();
        this.rabbitTemplate = rabbitTemplate;
        this.rabbitTemplate.setReplyTimeout(3000);
    }

    @PostMapping(value = "send", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> sendMailRegistration(@RequestBody final Registration registration) throws ContractValidationException {
        registration.validate();

        try {
            rabbitTemplate.convertAndSend(queueName, objectMapper.writeValueAsString(registration));
            logger.info("New {} type message has sent to the queue {}", Registration.class.getSimpleName(), queueName);
            return ResponseEntity.ok().build();
        } catch (final Exception e) {
            logger.error(e.toString());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
