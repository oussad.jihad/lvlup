package fr.jihadoussad.lvlup.api.security;

import fr.jihadoussad.lvlup.api.context.LvlupApiContext;
import fr.jihadoussad.lvlup.api.utils.CookieUtils;
import fr.jihadoussad.tokenserver.contract.TokenService;
import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.tokenserver.contract.input.VerifyTokenInput;
import io.jsonwebtoken.Claims;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

public class AuthorizationFilter extends OncePerRequestFilter {

    private final Logger logger = LoggerFactory.getLogger(AuthorizationFilter.class);

    private final LvlupApiContext context;

    private final CookieUtils cookieUtils;

    private final TokenService tokenService;

    public AuthorizationFilter(final LvlupApiContext context, final CookieUtils cookieUtils, final TokenService tokenService) {
        this.context = context;
        this.cookieUtils = cookieUtils;
        this.tokenService = tokenService;
    }

    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain chain) throws ServletException, IOException {
        if (!request.getServletPath().matches("^/admin/.*$")) {
            chain.doFilter(request, response);
            return;
        }

        logger.info("Attempt to access {} resource with {} http method from {} address", request.getPathInfo(), request.getMethod(), request.getRemoteAddr());
        final Optional<Cookie> accessTokenCookie = cookieUtils.getAccessTokenCookieValue(request);

        if (accessTokenCookie.map(Cookie::getValue).isEmpty()) {
            logger.info("Access token not found. Attempt to identify the authority on this resource without token...");
            chain.doFilter(request, response);
            return;
        }

        logger.info("Token found. Process checking validity...");
        final Authentication authentication = getAuthentication(accessTokenCookie.map(Cookie::getValue).get());

        logger.info("Checking validity token finish. Attempt to identify the authority on this resource...");
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }

    private Authentication getAuthentication(final String accessToken) {
        try {
            final VerifyTokenInput input = new VerifyTokenInput();
            input.setSecretKey(context.getAccessSecretKey());
            input.setToken(accessToken);
            final Claims claims = tokenService.verifyToken(input);
            @SuppressWarnings("unchecked") final Collection<String> authorities = (Collection<String>) claims.get(context.getAuthoritiesName());
            final String login = claims.getSubject();
            logger.info("[SECURITY][AUTHORIZATION] Token valid. Welcome {}", login);
            return new UsernamePasswordAuthenticationToken(login, null, getAuthorities(authorities));
        } catch (final ContractValidationException e) {
            logger.info("[SECURITY][AUTHORIZATION] Token server checking access token validity and finish with following status: {}.", e.getMessage());
        }

        return null;
    }

    private Collection<SimpleGrantedAuthority> getAuthorities(final Collection<String> privileges) {
        return privileges
                .stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
    }
}
