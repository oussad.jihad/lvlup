package fr.jihadoussad.lvlup.api.utils;

import fr.jihadoussad.lvlup.api.context.LvlupApiContext;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseCookie;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Optional;

@Component
public class CookieUtils {

    private final LvlupApiContext context;

    private final Environment environment;

    public CookieUtils(final LvlupApiContext context, final Environment environment) {
        this.context = context;
        this.environment = environment;
    }

    public ResponseCookie createAccessTokenCookie(final String token) {
        final boolean secure = Arrays.stream(environment.getActiveProfiles()).noneMatch(profile -> profile.equals("dev"));
        return ResponseCookie.from(context.getAccessTokenCookieName(), token)
                .httpOnly(true)
                .path("/")
                .secure(secure)
                .sameSite(secure ? "None": "Lax")
                .maxAge(context.getAccessTokenExpiration().intValue() / 1000)
                .build();
    }

    public Optional<Cookie> getAccessTokenCookieValue(final HttpServletRequest request) {
        return Arrays.stream(Optional.ofNullable(request.getCookies()).orElse(new Cookie[0]))
                .filter(cookie -> context.getAccessTokenCookieName().equals(cookie.getName())).findFirst();
    }
}
