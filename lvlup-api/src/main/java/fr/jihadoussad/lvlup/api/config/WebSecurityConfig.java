package fr.jihadoussad.lvlup.api.config;

import fr.jihadoussad.lvlup.api.context.LvlupApiContext;
import fr.jihadoussad.lvlup.api.security.AuthenticationFilter;
import fr.jihadoussad.lvlup.api.security.AuthorizationFilter;
import fr.jihadoussad.lvlup.api.security.LogoutSuccessHandler;
import fr.jihadoussad.lvlup.api.security.UserService;
import fr.jihadoussad.lvlup.api.utils.CookieUtils;
import fr.jihadoussad.tokenserver.contract.TokenService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserService userService;

    private final PasswordEncoder passwordEncoder;

    private final TokenService tokenService;

    private final LvlupApiContext context;

    private final CookieUtils cookieUtils;

    public WebSecurityConfig(final UserService userService, final PasswordEncoder passwordEncoder,
                             final TokenService tokenService, final LvlupApiContext context, final CookieUtils cookieUtils) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.tokenService = tokenService;
        this.context = context;
        this.cookieUtils = cookieUtils;
    }

    @Bean
    public CorsFilter corsFilter() {
        final CorsConfiguration configuration = new CorsConfiguration();
        configuration.addAllowedOrigin("http://" + context.getFrontUrl());
        configuration.addAllowedOrigin("https://" + context.getFrontUrl());
        configuration.addAllowedOrigin("http://www." + context.getFrontUrl());
        configuration.addAllowedOrigin("https://www." + context.getFrontUrl());
        configuration.addAllowedHeader("*");
        configuration.addAllowedMethod("*");
        configuration.setAllowCredentials(true);
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return new CorsFilter(source);
    }

    @Override
    public void configure(final WebSecurity web) {
        context.getWebUrlIgnoring().forEach(url -> web.ignoring()
                .antMatchers(url));
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        // Disable http basic
        http.httpBasic().disable();

        // Session policy
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // Activate Cross Origin Resource Sharing
        http.cors();

        // Activate Cross Site Request Forgery
        http.csrf().disable();

        // Authorize admin request
        http.addFilterBefore(new AuthorizationFilter(context, cookieUtils, tokenService), UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers("/admin/**").hasAnyAuthority("ROLE_ADMIN")
                .anyRequest().permitAll();

        // Authenticate config
        http.addFilter(new AuthenticationFilter(authenticationManagerBean(), context, cookieUtils, tokenService, userService));

        // Logout handler
        http.logout()
                .logoutSuccessHandler(new LogoutSuccessHandler(cookieUtils, tokenService))
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .deleteCookies(context.getAccessTokenCookieName());
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
