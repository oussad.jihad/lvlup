package fr.jihadoussad.lvlup.api.security;

import fr.jihadoussad.lvlup.api.context.LvlupApiContext;
import fr.jihadoussad.lvlup.api.utils.CookieUtils;
import fr.jihadoussad.tokenserver.contract.TokenService;
import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.tokenserver.contract.input.GenerateTokenInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);

    private final AuthenticationManager authenticationManager;

    private final LvlupApiContext context;

    private final CookieUtils cookieUtils;

    private final TokenService tokenService;

    private final UserService userService;

    public AuthenticationFilter(final AuthenticationManager authenticationManager, final LvlupApiContext context,
                                final CookieUtils cookieUtils, final TokenService tokenService, final UserService userService) {
        this.authenticationManager = authenticationManager;
        this.context = context;
        this.cookieUtils = cookieUtils;
        this.tokenService = tokenService;
        this.userService = userService;
    }

    @Override
    public Authentication attemptAuthentication(final HttpServletRequest request, final HttpServletResponse response) throws AuthenticationException {
        final String username = request.getParameter("username");
        final String password = request.getParameter("password");
        logger.info("Try to authenticate with username = {} and password = *******", username);
        if (userService.maxRetryAuthentication(username)) {
            logger.info("Max retry authentication, please contact your administrator.");
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("", ""));
        }

        return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    }

    @Override
    protected void successfulAuthentication(final HttpServletRequest request, final HttpServletResponse response, final FilterChain chain, final Authentication authResult) throws IOException, ServletException {
        final User user = (User) authResult.getPrincipal();
        try {
            userService.clearMaxRetry(user.getUsername());
            final GenerateTokenInput input = new GenerateTokenInput();
            input.setIssuer(user.getUsername());
            input.setSubject(context.getSubjectName());
            input.setExpiration(context.getAccessTokenExpiration());
            input.setSecretKey(context.getAccessSecretKey());
            input.setClaims(Map.of(context.getAuthoritiesName(), user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList())));
            response.addHeader(HttpHeaders.SET_COOKIE, cookieUtils.createAccessTokenCookie(tokenService.generateToken(input)).toString());
            logger.info("User {} successfully authenticate. {} cookie generated.", user.getUsername(), context.getAccessTokenCookieName());
        } catch (final ContractValidationException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    protected void unsuccessfulAuthentication(final HttpServletRequest request, final HttpServletResponse response, final AuthenticationException failed) throws IOException, ServletException {
        userService.unsuccessfulAuthentication(request.getParameter("username"));
        super.unsuccessfulAuthentication(request, response, failed);
    }
}
