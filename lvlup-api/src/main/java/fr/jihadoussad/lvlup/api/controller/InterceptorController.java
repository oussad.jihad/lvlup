package fr.jihadoussad.lvlup.api.controller;

import fr.jihadoussad.lvlup.contract.ContractValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class InterceptorController {

    private final Logger logger = LoggerFactory.getLogger(InterceptorController.class);

    @ExceptionHandler(value = ContractValidationException.class)
    public ResponseEntity<String> contractViolation(final ContractValidationException exception) {
        logger.warn(exception.toString());

        return ResponseEntity.status(exception.getHttpStatus()).contentType(MediaType.APPLICATION_JSON).body(exception.getMessage());
    }
}
