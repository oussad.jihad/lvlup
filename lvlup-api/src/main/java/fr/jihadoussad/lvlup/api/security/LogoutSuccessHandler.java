package fr.jihadoussad.lvlup.api.security;

import fr.jihadoussad.lvlup.api.utils.CookieUtils;
import fr.jihadoussad.tokenserver.contract.TokenService;
import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.tokenserver.contract.input.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogoutSuccessHandler implements org.springframework.security.web.authentication.logout.LogoutSuccessHandler {

    private final Logger logger = LoggerFactory.getLogger(LogoutSuccessHandler.class);

    private final CookieUtils cookieUtils;

    private final TokenService tokenService;

    public LogoutSuccessHandler(final CookieUtils cookieUtils, final TokenService tokenService) {
        this.cookieUtils = cookieUtils;
        this.tokenService = tokenService;
    }

    @Override
    public void onLogoutSuccess(final HttpServletRequest request, final HttpServletResponse response, final Authentication authentication) {
        final String accessToken = cookieUtils.getAccessTokenCookieValue(request).map(Cookie::getValue).orElse(null);

        if (accessToken == null) {
            logger.info("Access token cookie not found !");
            return;
        }

        try {
            logger.info("Access token found !");
            final Token token = new Token();
            token.setToken(accessToken);
            tokenService.invalidateToken(token);
            logger.info("Access token invalidate");
        } catch (final ContractValidationException e) {
            logger.error(e.getMessage());
        }
    }
}
