package fr.jihadoussad.lvlup.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.jihadoussad.lvlup.core.entities.*;
import fr.jihadoussad.lvlup.core.repositories.FormationRepository;
import fr.jihadoussad.lvlup.core.repositories.SessionRepository;
import fr.jihadoussad.lvlup.api.context.LvlupApiContext;
import fr.jihadoussad.lvlup.core.repositories.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.FileInputStream;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;

@SpringBootApplication
@ComponentScan(basePackages = "fr.jihadoussad")
@EntityScan(basePackages = {"fr.jihadoussad.lvlup.core.*", "fr.jihadoussad.tokenserver.core.*"})
@EnableJpaRepositories(basePackages = {"fr.jihadoussad.lvlup.core.*", "fr.jihadoussad.tokenserver.core.*"})
public class LvlUpApplication extends SpringBootServletInitializer {

    public static final String USERNAME = "admin";
    public static final String PASSWORD = "admin";
    public static final String RESET_PASSWORD_TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJkOWFiMGYyMjBmNDY0ZWUxYWZiNzBkYTM5NTlkNDIyNiIsImlzcyI6Im5vcmVwbHlAbHZsdXBjby5mciIsImlhdCI6MTY0Mjk2MzM4MCwibmJmIjoxNjQyOTYzMzgwLCJleHAiOjQ3OTY1NjMzODAsInN1YiI6ImFkbWluQHRlc3QuY29tIn0.3OHVaY9ln6n4OguxREXm68sx4ncXftQlneVocZgLhjU";
    public static final String EMAIL = "admin@test.com";
    public static final String MONTESSORI_NAME = "Montessori";
    public static final String MONTESSORI_PHRASE = "Acquérez les compétences indispensables à l'accompagnement des tout-petits dans leur développement sensoriel, cognitif et moteur";
    public static final int MONTESSORI_PRICE = 2490;
    public static final String MONTESSORI_PLACE = "Dubaï";
    public static final String CREATION_ENTREPRISE_NAME = "Création d'entreprise";
    public static final String CREATION_ENTREPRISE_PHRASE = "Intégrez tous les éléments incontournables à la création et à la gestion d'entreprise";
    public static final int CREATION_ENTREPRISE_PRICE = 990;
    public static final String CREATION_ENTREPRISE_PLACE = "Distanciel";
    public static final String FIRST_PARAGRAPHE = "Un paragraphe";
    public static final String SECOND_PARAGRAPHE = "...";

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    ObjectMapper objectMapper() {
        return Jackson2ObjectMapperBuilder.json().modules(new JavaTimeModule()).featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS).build();
    }

    @Override
    protected SpringApplicationBuilder configure(final SpringApplicationBuilder builder) {
        return builder.sources(LvlUpApplication.class);
    }

    public static void main(final String[] args) {
        SpringApplication.run(LvlUpApplication.class, args);
    }

    @Bean
    @Profile({"dev", "test"})
    public CommandLineRunner loadDevData(final FormationRepository formationRepository, final SessionRepository sessionRepository,
                                         final UserRepository userRepository, final LvlupApiContext context) {
        return (args) -> {
            // Role admin
            final Role adminRole = new Role();
            adminRole.setAuthority("ROLE_ADMIN");

            // User admin
            final User user = new User();
            user.setLogin(USERNAME);
            user.setEmail(EMAIL);
            user.setPassword(passwordEncoder().encode(PASSWORD));
            user.getRoles().add(adminRole);
            user.setCreationDate(LocalDateTime.now());
            userRepository.saveAndFlush(user);

            //Points
            final Point firstMontessoriParagraphe = new Point();
            firstMontessoriParagraphe.setContent(FIRST_PARAGRAPHE);
            final Point secondMontessoriParagraphe = new Point();
            secondMontessoriParagraphe.setContent(SECOND_PARAGRAPHE);
            final Point firstCreationEntrepriseParagraphe = new Point();
            firstCreationEntrepriseParagraphe.setContent(FIRST_PARAGRAPHE);
            final Point secondCreationEntrepriseParagraphe = new Point();
            secondCreationEntrepriseParagraphe.setContent(SECOND_PARAGRAPHE);

            // Montessori Pdf
            final Pdf montessoriPdf = new Pdf();
            final byte[] montessoriPdfStream = new FileInputStream(context.getBaseResourceUrl() + "/pdf/plaquette-formation-montessori.pdf").readAllBytes();
            montessoriPdf.setFile(montessoriPdfStream);

            // Montessori image
            final Image montessoriImage = new Image();
            final InputStream montessoriImageStream = new FileInputStream(context.getBaseResourceUrl() + "/img/Montessori.jpg");
            montessoriImage.setFile(montessoriImageStream.readAllBytes());

            // Montessori
            final Formation montessori = new Formation();
            montessori.setNom(MONTESSORI_NAME);
            montessori.setPhrase(MONTESSORI_PHRASE);
            montessori.setPrix(MONTESSORI_PRICE);
            montessori.setImage(montessoriImage);
            montessori.setPdf(montessoriPdf);
            montessori.addPoint(firstMontessoriParagraphe, secondMontessoriParagraphe);
            formationRepository.save(montessori);

            // Création entreprise Pdf
            final Pdf creationEntreprisePdf = new Pdf();
            final byte[] creationEntreprisePdfStream = new FileInputStream(context.getBaseResourceUrl() + "/pdf/plaquette-formation-creation-entreprise.pdf").readAllBytes();
            creationEntreprisePdf.setFile(creationEntreprisePdfStream);

            // Création entreprise image
            final Image creationEntrepriseImage = new Image();
            final InputStream creationEntrepriseImageStream = new FileInputStream(context.getBaseResourceUrl() + "/img/creation-entreprise.jpg");
            creationEntrepriseImage.setFile(creationEntrepriseImageStream.readAllBytes());

            // Création d'entreprise
            final Formation creationEntreprise = new Formation();
            creationEntreprise.setNom(CREATION_ENTREPRISE_NAME);
            creationEntreprise.setPhrase(CREATION_ENTREPRISE_PHRASE);
            creationEntreprise.setPrix(CREATION_ENTREPRISE_PRICE);
            creationEntreprise.setImage(creationEntrepriseImage);
            creationEntreprise.setPdf(creationEntreprisePdf);
            creationEntreprise.addPoint(firstCreationEntrepriseParagraphe, secondCreationEntrepriseParagraphe);
            formationRepository.save(creationEntreprise);

            final Session montessoriSession = new Session();
            montessoriSession.setDate(LocalDate.now());
            montessoriSession.setFormation(montessori);
            montessoriSession.setLieu(MONTESSORI_PLACE);
            sessionRepository.save(montessoriSession);

            final Session creationEntrepriseSession = new Session();
            creationEntrepriseSession.setDate(LocalDate.now().plusMonths(1));
            creationEntrepriseSession.setFormation(creationEntreprise);
            creationEntrepriseSession.setLieu(CREATION_ENTREPRISE_PLACE);
            sessionRepository.save(creationEntrepriseSession);

            formationRepository.flush();
            sessionRepository.flush();
        };
    }
}
