package fr.jihadoussad.lvlup.api.mapper;

import fr.jihadoussad.lvlup.contract.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class SessionMapper {

    private final Logger logger = LoggerFactory.getLogger(SessionMapper.class);

    private final FormationMapper formationMapper;

    public SessionMapper(final FormationMapper formationMapper) {
        this.formationMapper = formationMapper;
    }

    public Session coreToContract(final fr.jihadoussad.lvlup.core.entities.Session coreSession) {
        logger.debug("Map follow core session into contract: {}", coreSession.toString());
        final Session contractSession = new Session();
        contractSession.setId(coreSession.getId());
        contractSession.setDate(coreSession.getDate());
        contractSession.setLieu(coreSession.getLieu());
        contractSession.setFormation(formationMapper.coreToContract(coreSession.getFormation()));
        logger.debug("Contract session build: {}", contractSession);

        return contractSession;
    }

    public fr.jihadoussad.lvlup.core.entities.Session contractToCore(final Session contractSession) {
        final fr.jihadoussad.lvlup.core.entities.Session coreSession = new fr.jihadoussad.lvlup.core.entities.Session();
        coreSession.setDate(contractSession.getDate());
        coreSession.setLieu(contractSession.getLieu());

        return coreSession;
    }
}
