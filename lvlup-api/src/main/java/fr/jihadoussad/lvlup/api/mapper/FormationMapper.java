package fr.jihadoussad.lvlup.api.mapper;

import fr.jihadoussad.lvlup.contract.Formation;
import fr.jihadoussad.lvlup.core.entities.Image;
import fr.jihadoussad.lvlup.core.entities.Pdf;
import fr.jihadoussad.lvlup.core.entities.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class FormationMapper {

    private final Logger logger = LoggerFactory.getLogger(FormationMapper.class);

    public Formation coreToContract(final fr.jihadoussad.lvlup.core.entities.Formation coreFormation) {
        logger.debug("Map follow core formation into contract: {}", coreFormation.toString());
        final Formation contractFormation = new Formation();
        contractFormation.setId(coreFormation.getId());
        contractFormation.setNom(coreFormation.getNom());
        contractFormation.setPhrase(coreFormation.getPhrase());
        contractFormation.setImageId(coreFormation.getImage().getId());
        contractFormation.setPdfId(coreFormation.getPdf().getId());
        contractFormation.setPoints(coreFormation.getPoints().stream().map(Point::getContent).collect(Collectors.toList()));
        contractFormation.setPrix(coreFormation.getPrix());
        logger.debug("Contract formation build: {}", contractFormation);

        return contractFormation;
    }

    public fr.jihadoussad.lvlup.core.entities.Formation contractToCore(final Formation contractFormation, final Image image, final Pdf pdf) {
        logger.debug("Map follow contract formation into core: {}", contractFormation.toString());
        final fr.jihadoussad.lvlup.core.entities.Formation coreFormation = new fr.jihadoussad.lvlup.core.entities.Formation();
        coreFormation.setNom(contractFormation.getNom());
        coreFormation.setPhrase(contractFormation.getPhrase());
        coreFormation.setPrix(contractFormation.getPrix());
        contractFormation.getPoints().forEach(content -> {
            final Point point = new Point();
            point.setContent(content);
            coreFormation.addPoint(point);
        });
        coreFormation.setImage(image);
        coreFormation.setPdf(pdf);
        logger.debug("Core formation build: {}", contractFormation);

        return coreFormation;
    }
}
