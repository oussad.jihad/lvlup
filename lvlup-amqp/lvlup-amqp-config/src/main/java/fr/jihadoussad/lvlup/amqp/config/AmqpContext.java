package fr.jihadoussad.lvlup.amqp.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "lvlup-amqp")
public class AmqpContext {

    private String mailContact;

    private String mailNoReply;

    private String passwordRedirection;

    private String passwordQueueName;

    private String passwordSecretKey;

    private Long passwordTokenExpiration;

    private String registrationQueueName;

    public AmqpContext() {
        this.passwordTokenExpiration = 900000L; // 15 min
    }

    public String getPasswordRedirection() {
        return passwordRedirection;
    }

    public void setPasswordRedirection(final String passwordRedirection) {
        this.passwordRedirection = passwordRedirection;
    }

    public String getPasswordSecretKey() {
        return passwordSecretKey;
    }

    public void setPasswordSecretKey(final String passwordSecretKey) {
        this.passwordSecretKey = passwordSecretKey;
    }

    public Long getPasswordTokenExpiration() {
        return passwordTokenExpiration;
    }

    public void setPasswordTokenExpiration(final Long passwordTokenExpiration) {
        this.passwordTokenExpiration = passwordTokenExpiration;
    }

    public String getPasswordQueueName() {
        return passwordQueueName;
    }

    public void setPasswordQueueName(final String passwordQueueName) {
        this.passwordQueueName = passwordQueueName;
    }

    public String getRegistrationQueueName() {
        return registrationQueueName;
    }

    public void setRegistrationQueueName(final String registrationQueueName) {
        this.registrationQueueName = registrationQueueName;
    }

    public String getMailContact() {
        return mailContact;
    }

    public void setMailContact(String mailContact) {
        this.mailContact = mailContact;
    }

    public String getMailNoReply() {
        return mailNoReply;
    }

    public void setMailNoReply(final String mailNoReply) {
        this.mailNoReply = mailNoReply;
    }
}
