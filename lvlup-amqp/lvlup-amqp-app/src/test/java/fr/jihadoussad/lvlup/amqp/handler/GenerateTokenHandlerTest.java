package fr.jihadoussad.lvlup.amqp.handler;

import fr.jihadoussad.lvlup.amqp.config.AmqpContext;
import fr.jihadoussad.tokenserver.contract.TokenService;
import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(SpringExtension.class)
public class GenerateTokenHandlerTest {

    @TestConfiguration
    static class GenerateTokenHandlerConfigurationTest {

        @Bean
        GenerateTokenHandler generateTokenHandler() {
            return new GenerateTokenHandler(amqpContext(), tokenService);
        }

        @Bean
        AmqpContext amqpContext() {
            final AmqpContext context = new AmqpContext();
            context.setMailContact("contact@lvlup.fr");
            context.setMailNoReply("noreply@lvlup.fr");
            context.setPasswordSecretKey("mySecret");

            return context;
        }

        @MockBean
        TokenService tokenService;
    }

    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    private GenerateTokenHandler handler;

    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    private TokenService tokenService;

    @Test
    public void HandleWhenContractViolationTest() throws ContractValidationException {
        Mockito.when(tokenService.generateToken(any())).thenThrow(new ContractValidationException("", ""));
        assertThrows(ContractValidationException.class, () -> handler.handle("test@test.com"));
    }

    @Test
    public void HandleTest() throws ContractValidationException {
        Mockito.when(tokenService.generateToken(any())).thenReturn(anyString());
        handler.handle("test@test.com");
    }
}
