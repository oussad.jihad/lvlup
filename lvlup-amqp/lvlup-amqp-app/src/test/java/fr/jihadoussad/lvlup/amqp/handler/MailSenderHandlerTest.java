package fr.jihadoussad.lvlup.amqp.handler;

import fr.jihadoussad.lvlup.amqp.config.AmqpContext;
import fr.jihadoussad.lvlup.contract.Registration;
import fr.jihadoussad.mailserver.contract.input.MailSenderInput;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
public class MailSenderHandlerTest {

    @TestConfiguration
    static class MailSenderHandlerConfigurationTest {

        @Bean
        MailSenderHandler mailSenderHandler() {
            return new MailSenderHandler(amqpContext());
        }

        @Bean
        AmqpContext amqpContext() {
            final AmqpContext context = new AmqpContext();
            context.setMailContact("contact@lvlup.fr");
            context.setMailNoReply("noreply@lvlup.fr");
            context.setPasswordRedirection("https://lvlupco.fr/pasword/reset");

            return context;
        }
    }

    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    private MailSenderHandler handler;

    @Test
    public void handleRegistrationTest() {
        final Registration registration = new Registration();
        registration.setLastname("name");
        registration.setFirstname("firstname");
        registration.setEmail("test@test.com");
        registration.setPhoneNumber("0707070707");
        registration.setFormation("formation");
        registration.setSessionDate("décembre 2021");

        final MailSenderInput mailSenderInput = handler.handle(registration);
        assertThat(mailSenderInput).isNotNull();
        assertThat(mailSenderInput.getFrom()).isEqualTo("test@test.com");
        assertThat(mailSenderInput.getTo()).isEqualTo("contact@lvlup.fr");
        assertThat(mailSenderInput.getSubject()).isEqualTo("[Inscription] formation");
        assertThat(mailSenderInput.getText()).isNotBlank();
    }

    @Test
    public void handleResetPasswordTest() {
        final MailSenderInput mailSenderInput = handler.handle("test@test.com", "myToken");
        assertThat(mailSenderInput).isNotNull();
        assertThat(mailSenderInput.getFrom()).isEqualTo("noreply@lvlup.fr");
        assertThat(mailSenderInput.getTo()).isEqualTo("test@test.com");
        assertThat(mailSenderInput.getSubject()).isEqualTo("LVLUP - Mot de passe oublié");
        assertThat(mailSenderInput.getText()).isNotBlank();
    }
}
