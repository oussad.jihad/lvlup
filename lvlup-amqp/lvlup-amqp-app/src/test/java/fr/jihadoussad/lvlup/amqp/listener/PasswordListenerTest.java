package fr.jihadoussad.lvlup.amqp.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.lvlup.amqp.config.AmqpContext;
import fr.jihadoussad.lvlup.amqp.handler.GenerateTokenHandler;
import fr.jihadoussad.lvlup.amqp.handler.MailSenderHandler;
import fr.jihadoussad.mailserver.contract.EmailService;
import fr.jihadoussad.mailserver.provider.EmailServiceMock;
import fr.jihadoussad.tokenserver.contract.TokenService;
import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
public class PasswordListenerTest {

    @TestConfiguration
    static class GenerateTokenHandlerConfigurationTest {

        @Bean
        PasswordListener passwordListener() {
            return new PasswordListener(emailService(), generateTokenHandler(), mailSenderHandler(), objectMapper());
        }

        @Bean
        GenerateTokenHandler generateTokenHandler() {
            return new GenerateTokenHandler(amqpContext(), tokenService);
        }

        @Bean
        MailSenderHandler mailSenderHandler() {
            return new MailSenderHandler(amqpContext());
        }

        @Bean
        AmqpContext amqpContext() {
            final AmqpContext context = new AmqpContext();
            context.setMailContact("contact@lvlup.fr");
            context.setMailNoReply("noreply@lvlup.fr");
            context.setPasswordSecretKey("mySecret");

            return context;
        }

        @Bean
        ObjectMapper objectMapper() {
            return Jackson2ObjectMapperBuilder.json().build();
        }

        @Bean
        EmailService emailService() {
            return new EmailServiceMock();
        }

        @MockBean
        TokenService tokenService;
    }

    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    private PasswordListener listener;

    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    private ObjectMapper objectMapper;

    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    private TokenService tokenService;

    @Test
    public void receiveWrongResetPasswordRequestTest() {
        listener.receiveResetPasswordRequest("wrongResetPasswordRequest");
    }

    @Test
    public void receiveResetPasswordRequestWhenContractViolationTest() throws ContractValidationException, JsonProcessingException {
        Mockito.when(tokenService.generateToken(any())).thenThrow(new ContractValidationException("", ""));
        listener.receiveResetPasswordRequest(objectMapper.writeValueAsString("test@test.com"));
    }

    @Test
    public void receiveResetPasswordRequestTest() throws JsonProcessingException {
        listener.receiveResetPasswordRequest(objectMapper.writeValueAsString("test@test.com"));
    }

}
