package fr.jihadoussad.lvlup.amqp.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.lvlup.amqp.config.AmqpContext;
import fr.jihadoussad.lvlup.amqp.handler.MailSenderHandler;
import fr.jihadoussad.lvlup.contract.Registration;
import fr.jihadoussad.mailserver.contract.EmailService;
import fr.jihadoussad.mailserver.provider.EmailServiceMock;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class RegistrationListenerTest {

    @TestConfiguration
    static class RegistrationListenerConfigurationTest {

        @Bean
        public RegistrationListener registrationListener() {
            return new RegistrationListener(emailService(), mailSenderHandler(), objectMapper());
        }

        @Bean
        MailSenderHandler mailSenderHandler() {
            return new MailSenderHandler(amqpContext());
        }

        @Bean
        AmqpContext amqpContext() {
            final AmqpContext context = new AmqpContext();
            context.setMailContact("contact@lvlupco.fr");

            return context;
        }

        @Bean
        ObjectMapper objectMapper() {
            return Jackson2ObjectMapperBuilder.json().build();
        }

        @Bean
        EmailService emailService() {
            return new EmailServiceMock();
        }
    }

    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    private RegistrationListener listener;

    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void receiveWrongRegistrationTest() {
        listener.receiveRegistration("wrongRegistration");
    }

    @Test
    public void receiveEmptyRegistrationTest() throws JsonProcessingException {
        listener.receiveRegistration(objectMapper.writeValueAsString(new Registration()));
    }

    @Test
    public void receiveRegistrationTest() throws JsonProcessingException {
        final Registration registration = new Registration();
        registration.setFormation("formation");
        registration.setSessionDate("décembre 2021");
        registration.setLastname("name");
        registration.setFirstname("firstname");
        registration.setPhoneNumber("0707070707");
        registration.setEmail("test@test.com");
        listener.receiveRegistration(objectMapper.writeValueAsString(registration));
    }
}
