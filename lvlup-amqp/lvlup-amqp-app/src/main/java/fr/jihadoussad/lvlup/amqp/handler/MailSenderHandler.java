package fr.jihadoussad.lvlup.amqp.handler;

import fr.jihadoussad.lvlup.amqp.config.AmqpContext;
import fr.jihadoussad.lvlup.contract.Registration;
import fr.jihadoussad.mailserver.contract.input.MailSenderInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.StringJoiner;

@Component
public class MailSenderHandler {

    private final Logger logger = LoggerFactory.getLogger(MailSenderHandler.class);

    private final String mailContact;
    private final String mailNoReply;
    private final String passwordRedirection;

    public MailSenderHandler(final AmqpContext context) {
        this.mailContact = context.getMailContact();
        this.mailNoReply = context.getMailNoReply();
        this.passwordRedirection = context.getPasswordRedirection();
    }

    public MailSenderInput handle(final String email, final String token) {
        logger.debug("Email account received: {}", email);
        final MailSenderInput mailSenderInput = new MailSenderInput();
        mailSenderInput.setFrom(mailNoReply);
        mailSenderInput.setTo(email);
        mailSenderInput.setSubject("LVLUP - Mot de passe oublié");
        mailSenderInput.setText(generateResetPasswordContent(token));
        logger.debug("MailSender input build: {}", mailSenderInput);
        return mailSenderInput;
    }

    public MailSenderInput handle(final Registration registration) {
        logger.debug("Registration received: {}", registration.toString());
        final MailSenderInput mailSenderInput = new MailSenderInput();
        mailSenderInput.setFrom(registration.getEmail());
        mailSenderInput.setTo(mailContact);
        mailSenderInput.setSubject("[Inscription] " + registration.getFormation());
        mailSenderInput.setText(generateRegistrationContent(registration));
        logger.debug("MailSender input build: {}", mailSenderInput);
        return mailSenderInput;
    }

    private String generateResetPasswordContent(final String token) {
        return new StringJoiner("\n")
                .add("Bonjour,")
                .add("")
                .add("Veuillez cliquer sur le lien suivant afin de réinitialiser votre mot de passe: " + this.passwordRedirection + "?token=" + token)
                .add("Si vous n'êtes pas à l'origine de cette demande, merci de bien vouloir ignorer ce mail.")
                .add("")
                .add("Cordialement,")
                .add("L'équipe LVLUP")
                .toString();
    }

    private String generateRegistrationContent(final Registration registration) {
        return new StringJoiner("\n",
                "Nouvelle demande d'inscription pour la session " + registration.getFormation() + " prévu au mois de " + registration.getSessionDate() + ":\n\n", ".")
                .add("nom: " + registration.getLastname())
                .add("prénom: " + registration.getFirstname())
                .add("numéro de téléphone: " + registration.getPhoneNumber())
                .add("cpf: " + registration.getCpf() + "€")
                .toString();
    }
}
