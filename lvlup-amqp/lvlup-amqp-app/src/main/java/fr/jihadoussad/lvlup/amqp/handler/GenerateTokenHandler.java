package fr.jihadoussad.lvlup.amqp.handler;

import fr.jihadoussad.lvlup.amqp.config.AmqpContext;
import fr.jihadoussad.tokenserver.contract.TokenService;
import fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.tokenserver.contract.input.GenerateTokenInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class GenerateTokenHandler {

    private final Logger logger = LoggerFactory.getLogger(GenerateTokenHandler.class);

    private final AmqpContext context;
    private final TokenService tokenService;

    public GenerateTokenHandler(final AmqpContext context, final TokenService tokenService) {
        this.context = context;
        this.tokenService = tokenService;
    }

    public String handle(final String email) throws ContractValidationException {
        logger.debug("Try to generate token with following email account: {}", email);
        final GenerateTokenInput input = new GenerateTokenInput();
        input.setSubject(email);
        input.setIssuer(context.getMailNoReply());
        input.setExpiration(context.getPasswordTokenExpiration());
        input.setSecretKey(context.getPasswordSecretKey());
        final String token = tokenService.generateToken(input);
        logger.debug("Token successfully generated: {}", token);
        return token;
    }
}
