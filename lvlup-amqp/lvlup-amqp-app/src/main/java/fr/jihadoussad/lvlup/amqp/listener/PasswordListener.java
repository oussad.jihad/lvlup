package fr.jihadoussad.lvlup.amqp.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.lvlup.amqp.handler.GenerateTokenHandler;
import fr.jihadoussad.lvlup.amqp.handler.MailSenderHandler;
import fr.jihadoussad.mailserver.contract.EmailService;
import fr.jihadoussad.mailserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.mailserver.contract.input.MailSenderInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "${lvlup-amqp.password-queue-name}")
public class PasswordListener {

    private final Logger logger = LoggerFactory.getLogger(PasswordListener.class);

    private final EmailService emailService;
    private final GenerateTokenHandler generateTokenHandler;
    private final MailSenderHandler mailSenderHandler;
    private final ObjectMapper objectMapper;

    public PasswordListener(@Qualifier("email-service-prod") final EmailService emailService, final GenerateTokenHandler generateTokenHandler,
                            final MailSenderHandler mailSenderHandler, final ObjectMapper objectMapper) {
        this.emailService = emailService;
        this.generateTokenHandler = generateTokenHandler;
        this.mailSenderHandler = mailSenderHandler;
        this.objectMapper = objectMapper;
    }

    @RabbitHandler
    public void receiveResetPasswordRequest(final String jsonUserId) {
        try {
            final String email = objectMapper.readValue(jsonUserId, String.class);
            final String token = generateTokenHandler.handle(email);
            final MailSenderInput mailSenderInput = mailSenderHandler.handle(email, token);
            logger.info("New reset password account request message received. Sending email process in progress...");
            emailService.sendEmail(mailSenderInput);
        } catch (final ContractValidationException | fr.jihadoussad.tokenserver.contract.exceptions.ContractValidationException e) {
            logger.warn("Contract violation: {}. \n Process rollback", e.toString());
        } catch (final JsonProcessingException e) {
            logger.error(e.toString());
        }
    }
}
