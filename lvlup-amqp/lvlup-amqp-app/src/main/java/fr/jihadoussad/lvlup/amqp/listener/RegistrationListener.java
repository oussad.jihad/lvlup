package fr.jihadoussad.lvlup.amqp.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.lvlup.amqp.handler.MailSenderHandler;
import fr.jihadoussad.lvlup.contract.Registration;
import fr.jihadoussad.mailserver.contract.EmailService;
import fr.jihadoussad.mailserver.contract.exceptions.ContractValidationException;
import fr.jihadoussad.mailserver.contract.input.MailSenderInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "${lvlup-amqp.registration-queue-name}")
public class RegistrationListener {

    private final Logger logger = LoggerFactory.getLogger(RegistrationListener.class);

    private final EmailService emailService;
    private final MailSenderHandler mailSenderHandler;
    private final ObjectMapper objectMapper;

    public RegistrationListener(@Qualifier("email-service-prod") final EmailService emailService,
                                final MailSenderHandler mailSenderHandler,
                                final ObjectMapper objectMapper) {
        this.emailService = emailService;
        this.mailSenderHandler = mailSenderHandler;
        this.objectMapper = objectMapper;
    }

    @RabbitHandler
    public void receiveRegistration(final String jsonRegistration) {
        try {
            final MailSenderInput mailSenderInput = mailSenderHandler.handle(objectMapper.readValue(jsonRegistration, Registration.class));
            logger.info("New registration message received. Sending email process in progress...");
            emailService.sendEmail(mailSenderInput);
        } catch (final ContractValidationException e) {
            logger.warn("Contract violation: {}. \n Process rollback", e.toString());
        } catch (final JsonProcessingException e) {
            logger.error(e.toString());
        }
    }
}
