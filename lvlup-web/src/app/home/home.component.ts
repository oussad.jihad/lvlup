import { Component, OnInit } from '@angular/core';

import { DatePipe } from "@angular/common";

import { Formation } from "../model/formation";
import { Notification } from "../model/notification";
import { Session } from "../model/session";

import { AuthenticationService } from "../services/authentication.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  notification: Notification = new Notification();
  formations: Formation[] = [];
  sessions: Session[] = [];
  availableSession: Map<string, Map<string, Session[]>> = new Map<string, Map<string, Session[]>>();
  isLogged: boolean = false;

  constructor(private authenticationService: AuthenticationService, private datePipe: DatePipe) { }

  ngOnInit(): void {
    this.isLogged = this.authenticationService.isUserLogged();
  }

  notify(notification: Notification) {
    this.notification = notification;
  }

  setFormations(formations: Formation[]) {
    this.formations = formations;
  }

  setSessions(sessions: Session[]) {
    this.sessions = sessions;
    this.setSessionAvailable()
  }

  addSessionAvailable(session: Session) {
    this.sessions.push(session);
    this.setSessionAvailable();
  }

  deleteSessionAvailable(session: Session) {
    const index = this.sessions.findIndex(s => s.id == session.id);
    this.sessions.splice(index, 1);
    this.setSessionAvailable();
  }

  deleteSessionAvailableByFormation(formation: Formation) {
    this.sessions.filter((session, index) => {
      if (session.formation.id == formation.id) {
        this.sessions.splice(index, 1);
      }
    });
    this.setSessionAvailable();
  }

  private setSessionAvailable() {
    let availableSession = new Map<string, Map<string, Session[]>>();
    let sortedSessions = this.sessions.sort((a: Session, b: Session) => {
      return +new Date(a.date) - +new Date(b.date);
    });
    this.getYearAvailable(sortedSessions).forEach(year => {
      let sessionMonthByYear: Map<string, Session[]> = new Map<string, Session[]>();
      this.getMonthAvailableByYear(year, sortedSessions).forEach(month => {
        sessionMonthByYear.set(month, sortedSessions.filter(session =>
            this.datePipe.transform(session.date, "YYYY", undefined, "fr-FR") == year && this.datePipe.transform(session.date, "MMMM", undefined, "fr-FR") == month
          )
        );
      });
      availableSession.set(year, sessionMonthByYear);
    });
    this.availableSession = availableSession;
  }

  logout() {
    this.authenticationService.logout().subscribe({
      next: _ => this.isLogged = false,
      error: _ => {
        this.notification.content = `Une erreur s'est produite. Veuillez réessayer ultérieurement.`;
        this.notification.type = 'danger';
        this.notification.hidden = false;
      }
    });
  }

  private getMonthAvailableByYear(year: string, sessions: Session[]) {
    return sessions.filter(session => this.datePipe.transform(session.date, "YYYY", undefined, "fr-FR") == year)
      .map(session => this.datePipe.transform(session.date, "MMMM", undefined, "fr-FR") || '')
      .filter(function(elem, index, self) {
        return index === self.indexOf(elem);
      });
  }

  private getYearAvailable(sessions: Session[]) {
    return sessions.map(session => this.datePipe.transform(session.date, "YYYY", undefined, "fr-FR") || '')
      .filter(function(elem, index, self) {
        return index === self.indexOf(elem);
      });
  }
}
