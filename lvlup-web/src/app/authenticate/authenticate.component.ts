import { Component, OnInit, ViewChild } from '@angular/core';

import { Router } from "@angular/router";

import { NotificationHandler } from "../handler/notification-handler";

import { Notification } from "../model/notification";

import { AuthenticationService } from "../services/authentication.service";

@Component({
  selector: 'app-authenticate',
  templateUrl: './authenticate.component.html',
  styleUrls: ['./authenticate.component.css']
})
export class AuthenticateComponent implements OnInit {
  notificationHandler: NotificationHandler;

  @ViewChild("closeForgotPasswordModal") closeForgotPasswordModal: any;

  notification: Notification = new Notification();
  username: string = '';
  password: string = '';
  email: string = '';

  constructor(private authenticationService: AuthenticationService, private router: Router) {
    this.notificationHandler = new NotificationHandler(authenticationService, router);
  }

  ngOnInit(): void {
    this.notification.hidden = true;
  }

  login() {
    this.authenticationService.login(this.username, this.password).subscribe({
      next: _ => this.router.navigate(['/home']),
      error: response => this.notification = this.notificationHandler.handleError(response.status, 'identifiant ou mot de passe incorrect !')
    });
  }

  resetPassword() {
    this.authenticationService.resetPassword(this.email).subscribe({
      next: _ => this.notification = this.notificationHandler.handleInfo('Vous allez recevoir un email pour finaliser la réinitialisation de votre mot de passe.'),
      error: response => this.notification = this.notificationHandler.handleError(response.status, `Ce compte n'existe pas ou a été bloqué. Veuillez contacter votre administrateur.`)
    });
    this.closeForgotPasswordModal.nativeElement.click();
  }

  closeNotification() {
    this.notification.hidden = true;
  }

  clearEmail() {
    this.email = '';
  }
}
