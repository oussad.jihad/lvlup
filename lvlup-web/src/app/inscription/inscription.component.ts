import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

import { DatePipe, KeyValue } from "@angular/common";

import { NotificationHandler } from "../handler/notification-handler";

import { AuthenticationService } from "../services/authentication.service";
import { LvlupApiService } from "../services/lvlup-api.service";

import { Formation} from "../model/formation";
import { Inscription } from "../model/inscription";
import { Notification } from "../model/notification";
import { Session } from "../model/session";
import {Router} from "@angular/router";

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {
  notificationHandler: NotificationHandler;

  @ViewChild('closeRegistrationModal') closeRegistrationModal: any;
  @ViewChild('closeCreationModal') closeCreationModal: any

  @Input() isLogged!: boolean;
  @Input() formations!: Formation[];
  @Input() availableSession!: Map<string, Map<string, Session[]>>;
  @Output("addSession") addAvailableSessionEvent = new EventEmitter<Session>();
  @Output("deleteSession") deleteSessionEvent = new EventEmitter<Session>();
  @Output("setSessions") setAvailableSessionEvent = new EventEmitter<Session[]>();
  @Output("notification") notificationEvent = new EventEmitter<Notification>();

  // Disable map month order key
  keyOrder = (a: KeyValue<string, Session[]>, b: KeyValue<string, Session[]>): number => {
    return 0;
  }
  inscription: Inscription = new Inscription();
  session: Session = new Session();

  constructor(private authenticationService: AuthenticationService, private lvlupApiService: LvlupApiService,
              private datePipe: DatePipe, private router: Router) {
    this.notificationHandler = new NotificationHandler(authenticationService, router);
  }

  ngOnInit(): void {
    this.session.formation = new Formation();
    this.buildAvailableSessionView();
  }

  private buildAvailableSessionView() {
    this.lvlupApiService.getSessions().subscribe({
      next: sessions => {
        this.setAvailableSessionEvent.emit(sessions);
      },
      error: _ => {
        this.notificationHandler.handleError();
      }
    });
  }

  clearSession() {
    this.session = new Session();
    this.session.formation = new Formation();
  }

  getImageUrl(id: number) {
    return this.lvlupApiService.getImageUrl(id);
  }

  selectSession(session: Session) {
    this.inscription.formation = session.formation.nom;
    this.inscription.sessionDate = this.datePipe.transform(session.date, "MMMM y", undefined, "fr-FR") || '';
  }

  register() {
    this.lvlupApiService.register(this.inscription)
      .subscribe({
        next: _ => {
          this.notificationEvent.emit(this.notificationHandler.handleInfo(`Demande d'inscription envoyée. Vous serez prochainement contacter par notre équipe.`));
        },
        error: _ => this.notificationEvent.emit(this.notificationHandler.handleError())
      });
    this.closeRegistrationModal.nativeElement.click();
  }

  postSession() {
    const index = this.formations.findIndex(formation => formation.nom == this.session.formation.nom);
    this.session.formation.id = this.formations[index].id;
    this.lvlupApiService.postSession(this.session).subscribe({
      next: response => this.lvlupApiService.getSession(JSON.parse(response.body)).subscribe({
        next: session => {
          this.addAvailableSessionEvent.emit(session);
          this.notificationEvent.emit(this.notificationHandler.handleInfo(`Session ${session.formation.nom} le ${this.datePipe.transform(session.date, "dd MMMM y", undefined, "fr-FR")} créée.`));
        },
        error: _ => this.notificationEvent.emit(this.notificationHandler.handleError())
      }),
      error: response => this.notificationEvent.emit(this.notificationHandler.handleError(response.status))
    });
    this.closeCreationModal.nativeElement.click();
  }

  deleteSession(session: Session) {
    this.lvlupApiService.deleteSession(session.id).subscribe({
      next: _ => {
        this.deleteSessionEvent.emit(session);
        this.notificationEvent.emit(this.notificationHandler.handleInfo(`Session ${session.formation.nom} du ${this.datePipe.transform(session.date, "dd MMMM y", undefined, "fr-FR")} supprimée.`));
      },
      error: response => this.notificationEvent.emit(this.notificationHandler.handleError(response.status))
    })
  }
}
