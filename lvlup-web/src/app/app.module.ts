import { NgModule } from '@angular/core';

import { environment } from "../environments/environment";

import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { PdfViewerModule } from "ng2-pdf-viewer";

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { AboutComponent } from "./about/about.component";
import { AuthenticateComponent } from './authenticate/authenticate.component';
import { CgvComponent } from "./cgv/cgv.component";
import { ContactComponent } from './contact/contact.component';
import { FooterComponent } from './footer/footer.component';
import { FormationComponent } from './formation/formation.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from "./home/home.component";
import { InscriptionComponent } from './inscription/inscription.component';
import { MentionsLegalesComponent } from "./mentions-legales/mentions-legales.component";
import { NotFoundComponent } from './not-found/not-found.component';
import { PlaquetteFormationComponent } from './plaquette-formation/plaquette-formation.component';
import { QualiopiComponent } from './qualiopi/qualiopi.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

import { AuthenticationService } from "./services/authentication.service";
import { LvlupApiService } from "./services/lvlup-api.service";

import { FileValueAccessorDirective } from './directives/file-value-accessor.directive';
import { NavigateSectionDirective } from "./directives/navigate-section.directive";
import { RequireFileValidatorDirective } from "./directives/require-file-validator.directive";
import { SamePasswordValidatorDirective } from './directives/same-password-validator.directive';

import {platformBrowserDynamic} from "@angular/platform-browser-dynamic";

import { DatePipe, registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr, 'fr');

import { NgcCookieConsentConfig, NgcCookieConsentModule } from "ngx-cookieconsent";
const cookieConfig:NgcCookieConsentConfig = {
  "cookie": {
    "domain": environment.domain
  },
  "position": "bottom",
  "theme": "classic",
  "palette": {
    "popup": {
      "background": "#2D2D27",
        "text": "#ffffff",
        "link": "#ffffff"
    },
    "button": {
      "background": "#f1d600",
        "text": "#000000",
        "border": "transparent"
    }
  },
  "type": "info",
  "content": {
  "message": "Ce site web utilise des cookies pour vous assurer la meilleure expérience de navigation sur notre site.",
    "dismiss": "OK, j'ai compris!",
    "deny": "Refuser",
    "link": "Plus d'information",
    "href": "https://cookiesandyou.com",
    "policy": "Cookie Policy",
    "header": "Cookies sur le site!",
    "allow": "Autoriser les cookies"
  }
}

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    AuthenticateComponent,
    CgvComponent,
    ContactComponent,
    FooterComponent,
    FormationComponent,
    HeaderComponent,
    HomeComponent,
    InscriptionComponent,
    MentionsLegalesComponent,
    NavigateSectionDirective,
    NotFoundComponent,
    PlaquetteFormationComponent,
    QualiopiComponent,
    ResetPasswordComponent,
    FileValueAccessorDirective,
    RequireFileValidatorDirective,
    SamePasswordValidatorDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgcCookieConsentModule.forRoot(cookieConfig),
    PdfViewerModule,
    AppRoutingModule
  ],
  providers: [
    DatePipe,
    AuthenticationService,
    LvlupApiService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }

platformBrowserDynamic().bootstrapModule(AppModule).then(r => r);
