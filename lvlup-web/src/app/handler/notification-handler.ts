import { Router } from "@angular/router";

import { Notification } from "../model/notification";

import { AuthenticationService } from "../services/authentication.service";

export class NotificationHandler {

  constructor(private authenticationService: AuthenticationService, private router: Router) {}

  handleInfo(message: string): Notification {
    let notification = new Notification();
    notification.content = message;
    notification.type = 'info';
    notification.hidden = false;

    return notification;
  }

  handleError(status?: string | '500', message?: string | `Une erreur s'est produite. Veuillez réessayer ultérieurement.`): Notification {
    let notification = new Notification();
    notification.type = 'danger';
    notification.hidden = false;
    if (status == '403' && this.authenticationService.isUserLogged()) {
      localStorage.setItem('isLogged', 'false');
      notification.content = `Votre session a expiré. Merci de bien vouloir vous identifier à nouveau.`;
      this.router.navigate(['/login']).then();
    }
    notification.content = message!;

    return notification;
  }
}
