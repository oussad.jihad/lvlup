import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AboutComponent } from "./about/about.component";
import { AuthenticateComponent } from "./authenticate/authenticate.component";
import { CgvComponent } from "./cgv/cgv.component";
import { HomeComponent } from "./home/home.component";
import { MentionsLegalesComponent } from "./mentions-legales/mentions-legales.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { PlaquetteFormationComponent } from "./plaquette-formation/plaquette-formation.component";
import { ResetPasswordComponent } from "./reset-password/reset-password.component";

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'login', component: AuthenticateComponent },
  { path: 'cgv', component: CgvComponent },
  { path: 'mentions-legales', component: MentionsLegalesComponent },
  { path: 'plaquette-formation/:id', component: PlaquetteFormationComponent },
  { path: 'password/reset', component: ResetPasswordComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
