import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QualiopiComponent } from './qualiopi.component';

describe('QualiopiComponent', () => {
  let component: QualiopiComponent;
  let fixture: ComponentFixture<QualiopiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QualiopiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QualiopiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
