import { AfterViewInit, Component, Input, OnInit } from '@angular/core';

import { Notification } from "../model/notification";

declare var anime: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, AfterViewInit {

  @Input() notification!: Notification

  constructor() { }

  ngOnInit(): void {}

  closeNotification() {
    this.notification.hidden = true;
  }

  ngAfterViewInit(): void {
    let textWrapper = document.querySelector('.masthead-heading');
    textWrapper!.innerHTML = textWrapper!.textContent!.replace(/\S/g, "<span class='letter' style='display:inline-block;'>$&</span>");

    anime.timeline({loop: 1})
      .add({
        targets: '.masthead-heading .letter',
        scale: [0, 1],
        duration: 1500,
        elasticity: 600,
        delay: (el: any, i: number) => 150 * (i+1)
      });
  }
}
