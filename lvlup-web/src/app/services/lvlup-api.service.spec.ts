import { TestBed } from '@angular/core/testing';

import { LvlupApiService } from './lvlup-api.service';

describe('VisitorApiService', () => {
  let service: LvlupApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LvlupApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
