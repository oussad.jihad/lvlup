import {environment} from "../../environments/environment";

import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from "@angular/common/http";

import { Inscription } from "../model/inscription";
import { Formation } from "../model/formation";
import { Session } from "../model/session";

import { Observable } from "rxjs";

@Injectable()
export class LvlupApiService {

  private defaultHttpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'}),
    withCredentials: true
  };

  constructor(private http: HttpClient) {}

  register(inscription: Inscription): Observable<void> {
    return this.http.post<any>(`${environment.visitorServiceUrl}/send`, inscription, this.defaultHttpOptions);
  }

  getFormations(): Observable<Formation[]> {
    return this.http.get<Formation[]>(`${environment.visitorServiceUrl}/formations`, this.defaultHttpOptions);
  }

  getFormation(id: number): Observable<Formation> {
    return this.http.get<Formation>(`${environment.visitorServiceUrl}/formation/${id}`, this.defaultHttpOptions);
  }

  postFormation(formation: Formation, pdf: Blob, image: Blob): Observable<any> {
    return this.http.post<number>(`${environment.visitorServiceUrl}/admin/formation`, LvlupApiService.buildFormDataFormation(formation, pdf, image), { withCredentials: true, observe: 'response'});
  }

  setFormation(formation: Formation, pdf?: Blob, image?: Blob): Observable<any> {
    return this.http.put<void>(`${environment.visitorServiceUrl}/admin/formation`, LvlupApiService.buildFormDataFormation(formation, pdf, image), { withCredentials: true, observe: 'response'});
  }

  private static buildFormDataFormation(formation?: Formation, pdf?: Blob, image?: Blob): FormData {
    const formData = new FormData();
    const jsonFormation = new TextEncoder().encode(JSON.stringify(formation));
    formData.append("formation", new Blob([jsonFormation], {type: 'application/octet-stream'}));
    if (image !== undefined) {
      formData.append("image", new Blob([image], {type: 'image/jpeg'}));
    }
    if (pdf !== undefined) {
      formData.append("pdf", new Blob([pdf], {type: 'application/pdf'}));
    }

    return formData;
  }

  deleteFormation(id: number): Observable<any> {
    return this.http.delete<void>(`${environment.visitorServiceUrl}/admin/formation/${id}`, { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8'), withCredentials: true, observe: 'response'});
  }

  getImageUrl(id: number) {
    return `${environment.visitorServiceUrl}/image/${id}`;
  }

  getPdfUrl(id: number) {
    return `${environment.visitorServiceUrl}/pdf/${id}`;
  }

  getSessions(): Observable<Session[]> {
    return this.http.get<Session[]>(`${environment.visitorServiceUrl}/sessions`, this.defaultHttpOptions);
  }

  getSession(id: number): Observable<Session> {
    return this.http.get<Session>(`${environment.visitorServiceUrl}/session/${id}`, this.defaultHttpOptions);
  }

  postSession(session: Session): Observable<any> {
    return this.http.post<number>(`${environment.visitorServiceUrl}/admin/session`, session, { headers: new HttpHeaders({'Content-Type': 'application/json'}), withCredentials: true, observe: 'response'});
  }

  deleteSession(id: number): Observable<any> {
    return this.http.delete<void>(`${environment.visitorServiceUrl}/admin/session/${id}`, { headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8'), withCredentials: true, observe: 'response'});
  }
}
