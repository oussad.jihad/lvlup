import { Injectable } from '@angular/core';

import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";

import { Observable, tap } from "rxjs";

import { environment } from "../../environments/environment";

@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient) {}

  login(username: string, password: string): Observable<any> {
    let params = new HttpParams().set('username', username).set('password', password);
    return this.http.post<void>(`${environment.visitorServiceUrl}/login`, undefined, { params: params, withCredentials: true, observe: 'response' })
      .pipe(tap(_ => localStorage.setItem('isLogged', 'true')));
  }

  resetPassword(email: string): Observable<any> {
    let params = new HttpParams().set('email', email);
    return this.http.post<void>(`${environment.visitorServiceUrl}/password/reset`, undefined, { headers: new HttpHeaders({'Content-Type': 'application/json'}), params: params, withCredentials: true, observe: 'response'})
  }

  setPassword(password: string, token: string): Observable<any> {
    let params = new HttpParams().set('new', password).set('token', token);
    return this.http.put<void>(`${environment.visitorServiceUrl}/password`, undefined, { headers: new HttpHeaders({'Content-Type': 'application/json'}), params: params, withCredentials: true, observe: 'response'})
  }

  isUserLogged(): boolean {
    return localStorage.getItem('isLogged') == 'true';
  }

  logout(): Observable<void> {
    return this.http.post<void>(`${environment.visitorServiceUrl}/logout`, undefined, { withCredentials: true })
      .pipe(tap(_ => localStorage.setItem('isLogged', 'false')));
  }
}
