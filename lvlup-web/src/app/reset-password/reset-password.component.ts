import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

import { AuthenticationService } from "../services/authentication.service";

import { Notification } from "../model/notification";

import { NotificationHandler } from "../handler/notification-handler";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  notificationHandler: NotificationHandler;

  notification: Notification = new Notification();
  token: string = '';
  password: string = '';
  confirmPassword: string = '';

  constructor(private authenticationService: AuthenticationService, private route: ActivatedRoute, private router: Router) {
    this.notificationHandler = new NotificationHandler(authenticationService, router);
  }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(params => {
      if (params.get('token') == null) this.router.navigate(['not-found']).then();
      this.token = params.get('token')!;
    });
  }

  setPassword() {
    let message = 'Votre session a expiré ! Veuillez effectuer une nouvelle demande de réinitialisation.';
    if (this.token == null) {
      this.notification = this.notificationHandler.handleError('403', message);
    }

    this.authenticationService.setPassword(this.password, this.token!).subscribe({
      next: _ => this.router.navigate(['/login']).then(_ => this.notification = this.notificationHandler.handleInfo('Votre mot de passe a été réinitialisé avec succès ! Merci de bien vouloir vous authentifier.')),
      error: response => this.notification = this.notificationHandler.handleError(response.status, message)
    });
  }

  closeNotification() {
    this.notification.hidden = true;
  }
}
