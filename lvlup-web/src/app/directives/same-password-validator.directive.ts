import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from "@angular/forms";

@Directive({
  selector: '[appSamePasswordValidator]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: SamePasswordValidatorDirective,
    multi: true
  }]
})
export class SamePasswordValidatorDirective implements Validator {

  validate(group: AbstractControl): ValidationErrors | null {
    let password = group.parent?.get('newPassword')?.value;
    let confirmPassword = group.parent?.get('confirmPassword')?.value;
    return password === confirmPassword ? null : { 'notSame': true }
  }

}
