import { Directive } from "@angular/core";
import { FormControl, NG_VALIDATORS, ValidationErrors, Validator} from "@angular/forms";

@Directive({
  selector: '[appRequireFileValidator]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: RequireFileValidatorDirective,
    multi: true
  }]
})
export class RequireFileValidatorDirective implements Validator {

  validate(group: FormControl): ValidationErrors | null {
    return group.value == null || group.value.length == 0 ? { "required" : true } : null;
  }

}
