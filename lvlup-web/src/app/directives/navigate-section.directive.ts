import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appNavigateSection]'
})
export class NavigateSectionDirective {

  @Input() section!: HTMLElement;

  constructor(private elementRef: ElementRef) { }

  @HostListener('click') onClick() {
    this.elementRef.nativeElement.active = true;
    this.section.scrollIntoView({ block: 'start',  behavior: 'smooth', inline: 'nearest' });
    this.mousePointer();
  }

  @HostListener('mouseover') onMouseover() {
    this.mousePointer();
  }

  private mousePointer() {
    this.elementRef.nativeElement.style.cursor = 'pointer';
  }
}
