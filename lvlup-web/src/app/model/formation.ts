export class Formation {
  id: number = 0;
  nom: string = '';
  phrase: string = '';
  prix: number = 0;
  imageId: number = 0;
  pdfId: number = 0;
  points: string[] = [];
}

