export class Notification {
  hidden: boolean = true;
  type: string = '';
  content: string = '';
}
