import { Session } from "./session";

export class Inscription {
  formation: string= '';
  sessionDate: string = '';
  lastname: string = '';
  firstname: string = '';
  email: string = '';
  phoneNumber: string = '';
  cpf: number = 0;
  session: Session | any;
}
