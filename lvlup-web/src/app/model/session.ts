import { Formation } from "./formation";

export class Session {
  id: number = 0;
  date!: Date;
  lieu: string = '';
  formation!: Formation;
}
