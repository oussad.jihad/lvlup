import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

import { Router } from "@angular/router";

import { Formation } from "../model/formation";
import { Notification } from "../model/notification";

import { NotificationHandler } from "../handler/notification-handler";

import { AuthenticationService } from "../services/authentication.service";
import { LvlupApiService } from "../services/lvlup-api.service";

@Component({
  selector: 'app-formation',
  templateUrl: './formation.component.html',
  styleUrls: ['./formation.component.css']
})
export class FormationComponent implements OnInit {
  notificationHandler: NotificationHandler;

  @ViewChild('closeCreationModal') closeCreationModal: any;
  @ViewChild('closeModificationModal') closeModificationModal: any;

  @Input() registrationSection!: HTMLElement;
  @Input() isLogged!: boolean;
  @Output("notification") notificationEvent = new EventEmitter<Notification>();
  @Output("formations") formationEvent = new EventEmitter<Formation[]>();
  @Output("sessions") sessionEvent = new EventEmitter<Formation>();

  formations!: Formation[];
  formation: Formation = new Formation();
  point: string = '';
  pdfFile?: Blob;
  imageFile?: Blob;

  constructor(private authenticationService: AuthenticationService, private lvlupApiService: LvlupApiService, private router: Router) {
    this.notificationHandler = new NotificationHandler(authenticationService, router);
  }

  ngOnInit(): void {
    this.lvlupApiService.getFormations().subscribe({
      next: formations => {
        this.formations = formations.sort((a: Formation, b: Formation) => {
          return a.id - b.id;
        });
        this.formationEvent.emit(formations);
      },
      error: _ => this.notificationEvent.emit(this.notificationHandler.handleError())
    });
  }

  getImageUrl(id: number) {
    return this.lvlupApiService.getImageUrl(id);
  }

  selectFormation(formation: Formation) {
    this.formation.id = formation.id
    this.formation.nom = formation.nom;
    this.formation.phrase = formation.phrase;
    this.formation.points = formation.points.slice();
    this.formation.prix = formation.prix;
    this.formation.pdfId = formation.pdfId;
    this.formation.imageId = formation.imageId;
  }

  clearFormation() {
    this.formation = new Formation();
    this.pdfFile = undefined;
    this.imageFile = undefined;
  }

  addPoint() {
    this.formation.points.push(this.point);
    this.point = '';
  }

  removePoint(index: number) {
    this.formation.points.splice(index, 1);
  }

  postFormation() {
    this.lvlupApiService.postFormation(this.formation, this.pdfFile!, this.imageFile!).subscribe({
      next: response => {
        this.lvlupApiService.getFormation(JSON.parse(response.body)).subscribe({
          next: newFormation => {
            this.formations.push(newFormation);
            this.notificationEvent.emit(this.notificationHandler.handleInfo(`Formation ${this.formation.nom} créée.`));
          },
          error: _ => this.notificationEvent.emit(this.notificationHandler.handleError())
        });
      },
      error: response => this.notificationEvent.emit(this.notificationHandler.handleError(response.status))
    });
    this.closeCreationModal.nativeElement.click();
  }

  setFormation() {
    this.lvlupApiService.setFormation(this.formation, this.pdfFile, this.imageFile).subscribe({
      next: _ => {
        this.lvlupApiService.getFormation(this.formation.id).subscribe({
          next: formation => {
            const index = this.formations.findIndex(f => f.id == formation.id);
            this.formations[index] = formation;
            this.notificationEvent.emit(this.notificationHandler.handleInfo(`Formation ${formation.nom} modifiée.`));
          },
          error: _ => this.notificationEvent.emit(this.notificationHandler.handleError())
        });
      },
      error: response => this.notificationHandler.handleError(response.status)
    });
    this.closeModificationModal.nativeElement.click();
  }

  deleteFormation(id: number) {
    this.lvlupApiService.deleteFormation(id).subscribe({
      next: _ => {
        const index = this.formations.findIndex(formation => formation.id == id);
        this.sessionEvent.emit(this.formations[index]);
        this.formations.splice(index, 1);
        this.formationEvent.emit(this.formations);
        this.notificationEvent.emit(this.notificationHandler.handleInfo(`Formation supprimée.`));
      },
      error: response => this.notificationHandler.handleError(response.status)
    });
    this.closeModificationModal.nativeElement.click();
  }
}
