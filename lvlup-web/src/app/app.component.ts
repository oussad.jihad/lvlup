import { Component, OnInit } from '@angular/core';

import {
  NgcCookieConsentService,
} from "ngx-cookieconsent";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'lvlup';

  constructor(private ccService: NgcCookieConsentService){}

  ngOnInit() {}
}
