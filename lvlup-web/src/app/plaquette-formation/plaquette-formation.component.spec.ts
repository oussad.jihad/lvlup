import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaquetteFormationComponent } from './plaquette-formation.component';

describe('PlaquetteFormationComponent', () => {
  let component: PlaquetteFormationComponent;
  let fixture: ComponentFixture<PlaquetteFormationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlaquetteFormationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaquetteFormationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
