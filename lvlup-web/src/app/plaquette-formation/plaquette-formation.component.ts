import {Component, HostListener, OnInit} from '@angular/core';

import { ActivatedRoute } from "@angular/router";
import {LvlupApiService} from "../services/lvlup-api.service";

@Component({
  selector: 'app-plaquette-formation',
  templateUrl: './plaquette-formation.component.html',
  styleUrls: ['./plaquette-formation.component.css']
})
export class PlaquetteFormationComponent implements OnInit {

  pdfSrc: string = '';
  zoom: number = 1;

  constructor(private lvlupApiService: LvlupApiService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const pdfId = this.route.snapshot.paramMap.get('id');
    this.pdfSrc = this.lvlupApiService.getPdfUrl(Number(pdfId));
    this.pdfZoom();
  }

  @HostListener("window:resize", []) pdfZoom() {
    // lg (for laptops and desktops - screens equal to or greater than 1200px wide)
    // md (for small laptops - screens equal to or greater than 992px wide)
    // sm (for tablets - screens equal to or greater than 768px wide)
    // xs (for phones - screens less than 768px wide)

    if (window.innerWidth >= 1200) {
      this.zoom = 0.5; // lg
    } else if (window.innerWidth >= 992) {
      this.zoom = 0.6;//md
    } else if (window.innerWidth  >= 768) {
      this.zoom = 0.8;//sm
    } else if (window.innerWidth < 768) {
      this.zoom = 1;//xs
    }

  }

}
