const baseUrl = '//lvlupco-api.fr';

export const environment = {
  domain: 'lvlupco.fr',
  production: true,
  visitorServiceUrl: `${baseUrl}/visitor-service`
};
