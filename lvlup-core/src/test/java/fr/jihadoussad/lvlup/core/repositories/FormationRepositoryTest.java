package fr.jihadoussad.lvlup.core.repositories;

import fr.jihadoussad.lvlup.core.entities.Formation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.List;

import static fr.jihadoussad.lvlup.core.LvlupRepositoryApplication.*;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class FormationRepositoryTest {

    @Resource
    private FormationRepository formationRepository;

    @BeforeEach
    public void setUp() {
        formationRepository.findByNom(FORMATION_NAME).ifPresent(formation -> {
            formation.setPrix(PRICE);
            formationRepository.saveAndFlush(formation);
        });
    }

    @Test
    public void getAllFormationsTest() {
        final List<Formation> formations = formationRepository.findAll();

        assertThat(formations).isNotNull();
        assertThat(formations.size()).isEqualTo(1);
        final Formation formation = formations.get(0);
        assertThat(formation).isNotNull();
        assertThat(formation.getId()).isNotNull();
        assertThat(formation.getNom()).isEqualTo(FORMATION_NAME);
        assertThat(formation.getPhrase()).isEqualTo(PHRASE);
        assertThat(formation.getPrix()).isEqualTo(PRICE);
        assertThat(formation.getImage()).isNotNull();
        assertThat(formation.getPdf()).isNotNull();
    }

    @Test
    @Transactional
    public void getFormationByNameTest() {
        final Formation formation = formationRepository.findByNom(FORMATION_NAME).orElse(null);

        assertThat(formation).isNotNull();
        assertThat(formation.toString()).isNotBlank();
    }

    @Test
    public void updateFormationTest() {
        formationRepository.findByNom(FORMATION_NAME).ifPresent(formation -> {
            formation.setPrix(1500);
            formationRepository.saveAndFlush(formation);
        });

        final Formation formation = formationRepository.findByNom(FORMATION_NAME).orElse(null);

        assertThat(formation)
                .isNotNull()
                .extracting(Formation::getPrix)
                .isEqualTo(1500);
    }
}
