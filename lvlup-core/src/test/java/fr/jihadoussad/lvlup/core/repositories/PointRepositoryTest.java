package fr.jihadoussad.lvlup.core.repositories;

import fr.jihadoussad.lvlup.core.entities.Point;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

import static fr.jihadoussad.lvlup.core.LvlupRepositoryApplication.FIRST_PARAGRAPHE;
import static fr.jihadoussad.lvlup.core.LvlupRepositoryApplication.SECOND_PARAGRAPHE;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class PointRepositoryTest {

    @Resource
    private PointRepository pointRepository;

    @Test
    public void getPointsTest() {
        final List<Point> points = pointRepository.findAll();

        assertThat(points).isNotNull().hasSize(2)
                .extracting(Point::getContent).containsExactly(FIRST_PARAGRAPHE, SECOND_PARAGRAPHE);
        assertThat(points.get(0).toString()).isNotBlank();
    }
}
