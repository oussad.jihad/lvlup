package fr.jihadoussad.lvlup.core.repositories;

import fr.jihadoussad.lvlup.core.entities.Pdf;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class PdfRepositoryTest {

    @Resource
    private PdfRepository pdfRepository;

    @Test
    public void getPdfsTest() {
        final List<Pdf> pdfs = pdfRepository.findAll();

        assertThat(pdfs).isNotNull().hasSize(1);
        assertThat(pdfs.get(0).getFile()).isNotNull();
        assertThat(pdfs.get(0).toString()).isNotBlank();
    }
}
