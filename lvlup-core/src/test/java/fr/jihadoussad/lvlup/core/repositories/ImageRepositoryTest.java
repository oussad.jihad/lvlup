package fr.jihadoussad.lvlup.core.repositories;

import fr.jihadoussad.lvlup.core.entities.Image;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class ImageRepositoryTest {

    @Resource
    private ImageRepository imageRepository;

    @Test
    public void getImagesTest() {
        final List<Image> images = imageRepository.findAll();

        assertThat(images).isNotNull().hasSize(1);
        assertThat(images.get(0).getFile()).isNotNull();
        assertThat(images.get(0).toString()).isNotBlank();
    }
}
