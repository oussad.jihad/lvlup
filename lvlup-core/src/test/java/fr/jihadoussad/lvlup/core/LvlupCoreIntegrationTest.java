package fr.jihadoussad.lvlup.core;

import fr.jihadoussad.lvlup.core.entities.Formation;
import fr.jihadoussad.lvlup.core.entities.Session;
import fr.jihadoussad.lvlup.core.repositories.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.List;

import static fr.jihadoussad.lvlup.core.LvlupRepositoryApplication.FORMATION_NAME;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class LvlupCoreIntegrationTest {

    @Resource
    private FormationRepository formationRepository;

    @Resource
    private ImageRepository imageRepository;

    @Resource
    private PdfRepository pdfRepository;

    @Resource
    private PointRepository pointRepository;

    @Resource
    private SessionRepository sessionRepository;

    @Autowired
    private CommandLineRunner commandLineRunner;

    @Test
    public void getSessionByFormationIdTest() {
        final List<Session> sessions = new ArrayList<>();

        formationRepository.findByNom(FORMATION_NAME)
                .flatMap(formation -> sessionRepository.getSessionAvailableByFormationId(formation.getId()))
                .ifPresent(sessions::addAll);

        assertThat(sessions).hasSize(2);
    }

    @Test
    public void deleteFormationTest() throws Exception {
        formationRepository.findByNom(FORMATION_NAME).ifPresent(formation -> formationRepository.delete(formation));

        assertThat(formationRepository.findByNom(FORMATION_NAME).isEmpty()).isTrue();
        assertThat(imageRepository.findAll()).isEmpty();
        assertThat(pdfRepository.findAll()).isEmpty();
        assertThat(pointRepository.findAll()).isEmpty();
        assertThat(sessionRepository.findAll()).isEmpty();

        commandLineRunner.run();
    }

    @Test
    public void deleteSessionTest() {
        sessionRepository.deleteAll();
        final List<Formation> formations = formationRepository.findAll();

        assertThat(formations).isNotNull().hasSize(1);
    }
}
