package fr.jihadoussad.lvlup.core.repositories;

import fr.jihadoussad.lvlup.core.entities.Formation;
import fr.jihadoussad.lvlup.core.entities.Session;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.List;

import static fr.jihadoussad.lvlup.core.LvlupRepositoryApplication.FORMATION_NAME;
import static fr.jihadoussad.lvlup.core.LvlupRepositoryApplication.PLACE;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class SessionRepositoryTest {

    @Resource
    private SessionRepository sessionRepository;

    @Test
    public void getAllSessionsTest() {
        final List<Session> sessions = sessionRepository.findAll();

        assertThat(sessions).isNotNull();
        assertThat(sessions.size()).isEqualTo(3);

        assertThat(sessions)
                .extracting(Session::getLieu)
                .extracting(PLACE::equals)
                .hasSize(3);

        assertThat(sessions)
                .extracting(Session::getFormation)
                .extracting(Formation::getNom)
                .extracting(FORMATION_NAME::equals)
                .hasSize(3);

        assertThat(sessions)
                .extracting(Session::getDate)
                .doesNotContainNull();

        assertThat(sessions)
                .extracting(Session::getId)
                .doesNotContainNull();
    }

    @Test
    @Transactional
    public void getAvailableSessionsTest() {
        final List<Session> availableSessions = sessionRepository.getSessionAvailable().orElse(List.of());

        assertThat(availableSessions).isNotEmpty();
        assertThat(availableSessions.size()).isEqualTo(2);
        assertThat(availableSessions.get(0).toString()).isNotBlank();
    }

}
