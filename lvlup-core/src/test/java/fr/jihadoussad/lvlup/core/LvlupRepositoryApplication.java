package fr.jihadoussad.lvlup.core;

import fr.jihadoussad.lvlup.core.entities.*;
import fr.jihadoussad.lvlup.core.repositories.FormationRepository;
import fr.jihadoussad.lvlup.core.repositories.SessionRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.io.FileInputStream;
import java.io.InputStream;
import java.time.LocalDate;

@SpringBootApplication
@ComponentScan(basePackages = "fr.jihadoussad.lvlup.core.*")
@EntityScan(basePackages = "fr.jihadoussad.lvlup.core.*")
@EnableJpaRepositories
public class LvlupRepositoryApplication {

    public static final String FIRST_PARAGRAPHE = "Un paragraphe";
    public static final String FORMATION_NAME = "Montessori";
    public static final String PHRASE = "Ma phrase";
    public static final String PLACE = "Dubaï";
    public static final int PRICE = 2500;
    public static final String SECOND_PARAGRAPHE = "...";

    public static void main(String[] args) {
        SpringApplication.run(LvlupRepositoryApplication.class);
    }

    @Bean
    public CommandLineRunner loadData(final FormationRepository formationRepository, final SessionRepository sessionRepository) {
        return (args) -> {
            //Points
            final Point firstParagraphe = new Point();
            firstParagraphe.setContent(FIRST_PARAGRAPHE);
            final Point secondParagraphe = new Point();
            secondParagraphe.setContent(SECOND_PARAGRAPHE);

            // Montessori image
            final Image image = new Image();
            final InputStream imageStream = new FileInputStream("src/test/resources/img/Montessori.jpg");
            image.setFile(imageStream.readAllBytes());

            // Montessori pdf
            final Pdf pdf = new Pdf();
            final InputStream pdfStream = new FileInputStream("src/test/resources/pdf/lvlup.pdf");
            pdf.setFile(pdfStream.readAllBytes());

            // Montessori formation
            final Formation montessori = new Formation();
            montessori.setNom(FORMATION_NAME);
            montessori.setPhrase(PHRASE);
            montessori.setPrix(PRICE);
            montessori.setImage(image);
            montessori.setPdf(pdf);
            montessori.addPoint(firstParagraphe, secondParagraphe);
            formationRepository.save(montessori);

            //Sessions
            final Session pastSession = new Session();
            pastSession.setDate(LocalDate.now().minusMonths(1));
            pastSession.setFormation(montessori);
            pastSession.setLieu(PLACE);
            sessionRepository.save(pastSession);

            final Session sessionNow = new Session();
            sessionNow.setDate(LocalDate.now());
            sessionNow.setFormation(montessori);
            sessionNow.setLieu(PLACE);
            sessionRepository.save(sessionNow);

            final Session futurSession = new Session();
            futurSession.setDate(LocalDate.now().plusMonths(1));
            futurSession.setFormation(montessori);
            futurSession.setLieu(PLACE);
            sessionRepository.save(futurSession);

            formationRepository.flush();
            sessionRepository.flush();
        };
    }
}
