package fr.jihadoussad.lvlup.core.entities;

import javax.persistence.*;
import java.util.Arrays;
import java.util.StringJoiner;

@Entity
@Table(name = "PDF")
public class Pdf {

    @Id
    @GeneratedValue
    @Column(name = "PDF_ID")
    private Long id;

    @Lob
    @Column(name = "PDF_FILE")
    private byte[] file;

    public Long getId() {
        return id;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(final byte[] file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Pdf.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("file=" + Arrays.toString(file))
                .toString();
    }
}
