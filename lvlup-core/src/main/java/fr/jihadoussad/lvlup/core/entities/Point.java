package fr.jihadoussad.lvlup.core.entities;

import javax.persistence.*;
import java.util.StringJoiner;

@Entity
@Table(name = "POINT")
public class Point {

    @Id
    @GeneratedValue
    @Column(name = "POINT_ID")
    private Long id;

    @Column(name = "POINT_CONTENT")
    private String content;

    public Long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Point.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("content='" + content + "'")
                .toString();
    }
}
