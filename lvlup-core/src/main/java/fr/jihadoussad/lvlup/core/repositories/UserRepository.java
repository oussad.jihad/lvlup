package fr.jihadoussad.lvlup.core.repositories;

import fr.jihadoussad.lvlup.core.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByLogin(final String login);

    Optional<User> findByEmail(final String email);
}
