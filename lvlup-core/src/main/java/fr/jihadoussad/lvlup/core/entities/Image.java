package fr.jihadoussad.lvlup.core.entities;

import javax.persistence.*;
import java.util.Arrays;
import java.util.StringJoiner;

@Entity
@Table(name = "IMAGE")
public class Image {

    @Id
    @GeneratedValue
    @Column(name = "IMAGE_ID")
    private Long id;

    @Lob
    @Column(name = "IMAGE_FILE")
    private byte[] file;

    public Long getId() {
        return id;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(final byte[] file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Image.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("file=" + Arrays.toString(file))
                .toString();
    }
}
