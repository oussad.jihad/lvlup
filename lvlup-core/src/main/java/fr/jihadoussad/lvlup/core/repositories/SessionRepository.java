package fr.jihadoussad.lvlup.core.repositories;

import fr.jihadoussad.lvlup.core.entities.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SessionRepository extends JpaRepository<Session, Long> {

    @Query("select session from Session  session where session.date >= current_date")
    Optional<List<Session>> getSessionAvailable();

    @Query("select session from Session  session where session.date >= current_date and session.formation.id= ?1")
    Optional<List<Session>> getSessionAvailableByFormationId(final Long id);
}
