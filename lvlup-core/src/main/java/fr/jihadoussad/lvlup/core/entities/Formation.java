package fr.jihadoussad.lvlup.core.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

@Entity
@Table(name = "FORMATION")
public class Formation {

    @Id
    @GeneratedValue
    @Column(name = "FORMATION_ID")
    private Long id;

    @Column(name = "FORMATION_NAME")
    private String nom;

    @Column(name = "FORMATION_PHRASE")
    private String phrase;

    @Column(name = "FORMATION_PRICE")
    private int prix;

    @OneToOne(orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="IMAGE_ID", nullable=false)
    private Image image;

    @OneToOne(orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="PDF_ID", nullable=false)
    private Pdf pdf;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Point> points;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "formation", fetch = FetchType.LAZY)
    private List<Session> sessions;

    public Formation() {
        this.points = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(final Image image) {
        this.image = image;
    }

    public Pdf getPdf() {
        return pdf;
    }

    public void setPdf(final Pdf pdf) {
        this.pdf = pdf;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void addPoint(final Point... point) {
        this.points.addAll(List.of(point));
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Formation.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("nom='" + nom + "'")
                .add("phrase='" + phrase + "'")
                .add("prix=" + prix)
                .add("points=" + points)
                .toString();
    }
}
