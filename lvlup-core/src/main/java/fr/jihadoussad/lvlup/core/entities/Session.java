package fr.jihadoussad.lvlup.core.entities;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.StringJoiner;

@Entity
@Table(name = "SESSION")
public class Session {

    @Id
    @GeneratedValue
    @Column(name = "SESSION_ID")
    private Long id;

    @Column(name = "SESSION_DATE")
    private LocalDate date;

    @Column(name = "SESSION_PLACE")
    private String lieu;

    @ManyToOne
    @JoinColumn(name = "FORMATION_ID", referencedColumnName = "FORMATION_ID", nullable = false)
    private Formation formation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(final LocalDate date) {
        this.date = date;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(final String lieu) {
        this.lieu = lieu;
    }

    public Formation getFormation() {
        return formation;
    }

    public void setFormation(final Formation formation) {
        this.formation = formation;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Session.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("date=" + date)
                .add("lieu='" + lieu + "'")
                .add("formation=" + formation)
                .toString();
    }
}
