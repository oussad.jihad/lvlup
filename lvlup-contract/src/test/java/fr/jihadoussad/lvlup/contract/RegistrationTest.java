package fr.jihadoussad.lvlup.contract;

import org.junit.jupiter.api.Test;

import static fr.jihadoussad.lvlup.contract.ResponseCode.*;
import static fr.jihadoussad.tools.api.response.ResponseCode.INVALID_MAIL;
import static fr.jihadoussad.tools.api.response.ResponseCode.MISSING_MANDATORY_FIELD;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RegistrationTest {

    private static final int BAD_REQUEST = 400;
    
    @Test
    void validateWhenFormationNullTest() {
        final Registration input = new Registration();

        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);

        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(exception.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "formation null");
        assertThat(exception.getHttpStatus()).isEqualTo(BAD_REQUEST);
    }

    @Test
    void validateWhenSessionDateNullTest() {
        final Registration input = new Registration();
        input.setFormation("formation");

        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);

        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(exception.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "session date null");
        assertThat(exception.getHttpStatus()).isEqualTo(BAD_REQUEST);
    }

    @Test
    void validateWhenLastnameNullTest() {
        final Registration input = new Registration();
        input.setFormation("formation");
        input.setSessionDate("décembre 2021");

        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);

        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(exception.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "lastname null");
        assertThat(exception.getHttpStatus()).isEqualTo(BAD_REQUEST);
    }

    @Test
    void validateWhenLastnameInvalidTest() {
        final Registration input = new Registration();
        input.setFormation("formation");
        input.setSessionDate("décembre 2021");
        input.setLastname("wrongLastname1");

        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);

        assertThat(exception.getCode()).isEqualTo(INVALID_LASTNAME.code);
        assertThat(exception.getHttpStatus()).isEqualTo(BAD_REQUEST);
    }

    @Test
    void validateWhenFirstnameNullTest() {
        final Registration input = new Registration();
        input.setFormation("formation");
        input.setSessionDate("décembre 2021");
        input.setLastname("lastname");

        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);

        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(exception.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "firstname null");
        assertThat(exception.getHttpStatus()).isEqualTo(BAD_REQUEST);
    }

    @Test
    void validateWhenFirstnameInvalidTest() {
        final Registration input = new Registration();
        input.setFormation("formation");
        input.setSessionDate("décembre 2021");
        input.setLastname("lastname");
        input.setFirstname("wrongFirstname1");

        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);

        assertThat(exception.getCode()).isEqualTo(INVALID_FIRSTNAME.code);
        assertThat(exception.getHttpStatus()).isEqualTo(BAD_REQUEST);
    }

    @Test
    void validateWhenEmailNullTest() {
        final Registration input = new Registration();
        input.setFormation("formation");
        input.setSessionDate("décembre 2021");
        input.setLastname("lastname");
        input.setFirstname("firstname");

        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);

        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(exception.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "email null");
        assertThat(exception.getHttpStatus()).isEqualTo(BAD_REQUEST);
    }

    @Test
    void validateWhenEmailInvalidTest() {
        final Registration input = new Registration();
        input.setFormation("formation");
        input.setSessionDate("décembre 2021");
        input.setLastname("lastname");
        input.setFirstname("firstname");
        input.setEmail("wrongEmail");

        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);

        assertThat(exception.getCode()).isEqualTo(INVALID_MAIL.code);
        assertThat(exception.getHttpStatus()).isEqualTo(BAD_REQUEST);
    }

    @Test
    void validateWhenPhoneNumberNullTest() {
        final Registration input = new Registration();
        input.setFormation("formation");
        input.setSessionDate("décembre 2021");
        input.setLastname("lastname");
        input.setFirstname("firstname");
        input.setEmail("test@test.fr");

        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);

        assertThat(exception.getCode()).isEqualTo(MISSING_MANDATORY_FIELD.code);
        assertThat(exception.getMessage()).isEqualTo(MISSING_MANDATORY_FIELD.message + "phone number null");
        assertThat(exception.getHttpStatus()).isEqualTo(BAD_REQUEST);
    }

    @Test
    void validateWhenPhoneNumberInvalidTest() {
        final Registration input = new Registration();
        input.setFormation("formation");
        input.setSessionDate("décembre 2021");
        input.setLastname("lastname");
        input.setFirstname("firstname");
        input.setEmail("test@test.fr");
        input.setPhoneNumber("+14155552671");

        final ContractValidationException exception = assertThrows(ContractValidationException.class, input::validate);

        assertThat(exception.getCode()).isEqualTo(INVALID_PHONE_NUMBER.code);
        assertThat(exception.getHttpStatus()).isEqualTo(BAD_REQUEST);
    }

    @Test
    void validateWithPlus33PhoneTest() throws ContractValidationException {
        final Registration input = new Registration();
        input.setFormation("formation");
        input.setSessionDate("décembre 2021");
        input.setLastname("lastname");
        input.setFirstname("firstname");
        input.setEmail("test@test.fr");
        input.setPhoneNumber("+33707070707");

        input.validate();
    }

    @Test
    void validateWith0033PhoneTest() throws ContractValidationException {
        final Registration input = new Registration();
        input.setFormation("formation");
        input.setSessionDate("décembre 2021");
        input.setLastname("lastname");
        input.setFirstname("firstname");
        input.setEmail("test@test.fr");
        input.setPhoneNumber("0033707070707");

        input.validate();
    }

    @Test
    void validateWithCpfSetTest() throws ContractValidationException {
        final Registration input = new Registration();
        input.setFormation("formation");
        input.setSessionDate("décembre 2021");
        input.setLastname("lastname");
        input.setFirstname("firstname");
        input.setEmail("test@test.fr");
        input.setPhoneNumber("0707070707");
        input.setCpf(2000);

        input.validate();

        assertThat(input.getCpf()).isEqualTo(2000);
    }

    @Test
    void validateTest() throws ContractValidationException {
        final Registration input = new Registration();
        input.setFormation("formation");
        input.setSessionDate("décembre 2021");
        input.setLastname("lastname");
        input.setFirstname("firstname");
        input.setEmail("test@test.fr");
        input.setPhoneNumber("0707070707");

        input.validate();

        assertThat(input.getFormation()).isEqualTo("formation");
        assertThat(input.getSessionDate()).isEqualTo("décembre 2021");
        assertThat(input.getLastname()).isEqualTo("lastname");
        assertThat(input.getFirstname()).isEqualTo("firstname");
        assertThat(input.getEmail()).isEqualTo("test@test.fr");
        assertThat(input.getPhoneNumber()).isEqualTo("0707070707");
        assertThat(input.getCpf()).isEqualTo(0);
        assertThat(input.toString()).isNotBlank();
    }
}
