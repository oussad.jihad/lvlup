package fr.jihadoussad.lvlup.contract;

/**
 * Validable
 */
public interface Validable {

    void validate() throws ContractValidationException;
}
