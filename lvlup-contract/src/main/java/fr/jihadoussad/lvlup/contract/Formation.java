package fr.jihadoussad.lvlup.contract;

import java.util.List;
import java.util.StringJoiner;

//TODO implements input validation
public class Formation {

    private Long id;

    private String nom;

    private String phrase;

    private Integer prix;

    private Long imageId;

    private Long pdfId;

    private List<String> points;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(final String nom) {
        this.nom = nom;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public Integer getPrix() {
        return prix;
    }

    public void setPrix(final Integer prix) {
        this.prix = prix;
    }

    public Long getImageId() {
        return imageId;
    }

    public void setImageId(final Long imageId) {
        this.imageId = imageId;
    }

    public Long getPdfId() {
        return pdfId;
    }

    public void setPdfId(final Long pdfId) {
        this.pdfId = pdfId;
    }

    public List<String> getPoints() {
        return points;
    }

    public void setPoints(final List<String> points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Formation.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("nom='" + nom + "'")
                .add("prix=" + prix)
                .add("imageId=" + imageId)
                .add("pdfId=" + pdfId)
                .add("points=" + points)
                .toString();
    }
}
