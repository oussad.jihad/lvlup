package fr.jihadoussad.lvlup.contract;

import fr.jihadoussad.tools.validators.CommonRegexValidator;
import fr.jihadoussad.tools.validators.InputValidator;

import java.util.StringJoiner;
import java.util.regex.Pattern;

import static fr.jihadoussad.lvlup.contract.ResponseCode.*;
import static fr.jihadoussad.tools.api.response.ResponseCode.INVALID_MAIL;
import static fr.jihadoussad.tools.api.response.ResponseCode.MISSING_MANDATORY_FIELD;

public class Registration implements Validable {

    private final InputValidator mailValidator;

    private final InputValidator nameValidator;

    private final InputValidator phoneValidator;

    private String formation;

    private String sessionDate;

    private String lastname;

    private String firstname;

    private String email;

    private String phoneNumber;

    private int cpf;

    public Registration() {
        this.mailValidator = InputValidator.getInstance(CommonRegexValidator.MAIL.pattern);
        this.nameValidator = InputValidator.getInstance(Pattern.compile("^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆŠŽ∂ð ,.'-]+$"));
        this.phoneValidator = InputValidator.getInstance(Pattern.compile("^(?:(?:\\+|00)33|0)\\d{9}$"));
    }

    @Override
    public void validate() throws ContractValidationException {
        if (formation == null) {
            throw new ContractValidationException(MISSING_MANDATORY_FIELD.message + "formation null", MISSING_MANDATORY_FIELD.code);
        }

        if (sessionDate == null) {
            throw new ContractValidationException(MISSING_MANDATORY_FIELD.message + "session date null", MISSING_MANDATORY_FIELD.code);
        }

        if (lastname == null) {
            throw new ContractValidationException(MISSING_MANDATORY_FIELD.message + "lastname null", MISSING_MANDATORY_FIELD.code);
        }

        if (!nameValidator.validate(lastname)) {
            throw new ContractValidationException(INVALID_LASTNAME.message, INVALID_LASTNAME.code);
        }

        if (firstname == null) {
            throw new ContractValidationException(MISSING_MANDATORY_FIELD.message + "firstname null", MISSING_MANDATORY_FIELD.code);
        }

        if (!nameValidator.validate(firstname)) {
            throw new ContractValidationException(INVALID_FIRSTNAME.message, INVALID_FIRSTNAME.code);
        }

        if (email == null) {
            throw new ContractValidationException(MISSING_MANDATORY_FIELD.message + "email null", MISSING_MANDATORY_FIELD.code);
        }

        if (!mailValidator.validate(email)) {
            throw new ContractValidationException(INVALID_MAIL.message, INVALID_MAIL.code);
        }

        if (phoneNumber == null) {
            throw new ContractValidationException(MISSING_MANDATORY_FIELD.message + "phone number null", MISSING_MANDATORY_FIELD.code);
        }

        if (!phoneValidator.validate(phoneNumber)) {
            throw new ContractValidationException(INVALID_PHONE_NUMBER.message, INVALID_PHONE_NUMBER.code);
        }
    }

    public String getFormation() {
        return formation;
    }

    public void setFormation(final String formation) {
        this.formation = formation;
    }

    public String getSessionDate() {
        return sessionDate;
    }

    public void setSessionDate(String sessionDate) {
        this.sessionDate = sessionDate;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(final String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(final String firstname) {
        this.firstname = firstname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getCpf() {
        return cpf;
    }

    public void setCpf(int cpf) {
        this.cpf = cpf;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Registration.class.getSimpleName() + "[", "]")
                .add("formation='" + formation + "'")
                .add("sessionDate='" + sessionDate + "'")
                .add("name='" + lastname + "'")
                .add("firstname='" + firstname + "'")
                .add("email='" + email + "'")
                .add("phoneNumber='" + phoneNumber + "'")
                .add("cpf=" + cpf)
                .toString();
    }
}
