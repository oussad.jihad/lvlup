package fr.jihadoussad.lvlup.contract;

public enum ResponseCode {

    INVALID_LASTNAME("22", "Lastname invalid !"),
    INVALID_FIRSTNAME("23", "Firstname invalid !"),
    INVALID_PHONE_NUMBER("24", "Phone number invalid !"),
    FORMATION_NOT_EXIST("25", "Formation not exist"),
    SESSION_NOT_EXIST("26", "Session not exist"),
    IMAGE_NOT_EXIST("27", "Image not exist"),
    PDF_NOT_EXIST("28", "Session not exist"),
    EMAIL_NOT_FOUND("29", "Email account not found"),
    ACCOUNT_BLOCKED("30", "Account is blocked ! Please contact your administrator");

    public final String code;
    public final String message;

    ResponseCode(final String code, final String message) {
        this.code = code;
        this.message = message;
    }
}
