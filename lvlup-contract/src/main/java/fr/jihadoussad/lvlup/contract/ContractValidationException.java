package fr.jihadoussad.lvlup.contract;

import java.util.StringJoiner;

/**
 * ContractValidationException
 */
public class ContractValidationException extends Exception {

    private static final int BAD_REQUEST = 400;

    private final String code;
    private final int httpStatus;

    public ContractValidationException(final String message, final String code) {
        super(message);
        this.code = code;
        this.httpStatus = BAD_REQUEST;
    }

    public ContractValidationException(final String message, final String code, final int httpStatus) {
        super(message);
        this.code = code;
        this.httpStatus = httpStatus;
    }

    public String getCode() {
        return code;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ContractValidationException.class.getSimpleName() + "[", "]")
                .add("code='" + code + "'")
                .add("message='" + super.getMessage() + "'")
                .add("httpStatus='" + httpStatus + "'")
                .toString();
    }
}
