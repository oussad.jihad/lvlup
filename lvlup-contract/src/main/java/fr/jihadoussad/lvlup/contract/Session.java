package fr.jihadoussad.lvlup.contract;

import java.time.LocalDate;
import java.util.StringJoiner;

// TODO implements input validation
public class Session {

    private Long id;

    private LocalDate date;

    private String lieu;

    private Formation formation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(final LocalDate date) {
        this.date = date;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(final String lieu) {
        this.lieu = lieu;
    }

    public Formation getFormation() {
        return formation;
    }

    public void setFormation(final Formation formation) {
        this.formation = formation;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Session.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("date=" + date)
                .add("lieu='" + lieu + "'")
                .add("formation=" + formation)
                .toString();
    }
}
